import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:job_portal/app/shared_prefrence/pref_keys.dart';
import 'package:job_portal/app/shared_prefrence/pref_manager.dart';

class HttpService {

  var _dio;

  Dio _getDio() {
    if (_dio == null) {
      _dio = new Dio();
      _dio.interceptors.add(
        InterceptorsWrapper(
          onRequest: (RequestOptions options, RequestInterceptorHandler handler) async {
            return handler.next(options); //continue
          },
          onResponse: (Response response, ResponseInterceptorHandler handler) async {
            return handler.next(response); // continue
          },
          onError: (DioError e, ErrorInterceptorHandler handler) async {
            return handler.next(e); //continue
          },
        ),
      );
      _dio.interceptors.add(LogInterceptor(responseBody: true));
    }


    return _dio;
  }

  Future<Response> getRequest(String route, {Map<String, String>? qp}) async {
    try {
      Response response = await _getDio().get(
        route,
        queryParameters: qp,
      );
      return response;
    } on DioError catch (e) {
      throw e;
    }
  }

  Future<Response> postRequest(
      String route, {
        Map<String, dynamic>? data,
        String? jsonData,
        bool isFormData = false,
        Function(int sent, int total)? onProgress,
      }) async {
    _getDio().options.headers = {
      "Content-Type": "application/json",
      // "Authorization": "Token ${PrefManager.getString(PrefKeys.AUTH_TOKEN_REGISTRATION)}"
    };

    try {
      Response response = await _getDio().post(
        route,
        data: isFormData
            ? FormData.fromMap(data!)
            : (jsonData ?? jsonEncode(data)),
        onSendProgress: (int sent, int total) {
          if (onProgress != null) onProgress(sent, total);
        },
      );
      print("$route : $data");
      return response;
    } on DioError catch (e) {
      throw e;
    }
  }

  uploadDocument(
      String route,
      String filePath,
      ) async {
    _getDio().options.headers = {
      "Content-Type": "application/json",
    };

    FormData formData = FormData.fromMap({
      "id": PrefManager.getString(PrefKeys.USER_ID),
      "image": await MultipartFile.fromFileSync(filePath,
          filename: "hello"),
    });
    try {
      print("Form Data : ${formData.fields}");
      var response = await _getDio().post(route, data: formData);
      return response;
    } on DioError catch (e) {
      throw e;
    }
  }

}