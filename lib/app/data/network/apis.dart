class Apis {

  static final BASE_URL = "https://mazharsany.xyz/jobs-today/api/";

  static final REGISTRATION = "${BASE_URL}registration";
  static final LOGIN = "${BASE_URL}login";
  static final UPDATE_PROFILE = "${BASE_URL}update-profile";
  static final UPDATE_IMAGE = "${BASE_URL}update-image";
  static final HOT_JOBS = "${BASE_URL}hot-jobs";
  static final UPDATE_CV = "${BASE_URL}update-cv";
  static final GET_CV = "${BASE_URL}get-cv";
  static final GET_QUIZ = "${BASE_URL}quiz";
  static final APPLY_JOBS = "${BASE_URL}apply-job";
  static final MY_APPLICATIONS= "${BASE_URL}my-applications";
  static final APPLICATION_STATUS_CHECK= "${BASE_URL}apply-confirmation";
  static final HIGHLIGHTED_USER= "${BASE_URL}highighted_user";
}