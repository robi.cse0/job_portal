// To parse this JSON data, do
//
//     final modelHighlightUser = modelHighlightUserFromJson(jsonString);

import 'package:meta/meta.dart';
import 'dart:convert';

ModelHighlightUser modelHighlightUserFromJson(String str) => ModelHighlightUser.fromJson(json.decode(str));

String modelHighlightUserToJson(ModelHighlightUser data) => json.encode(data.toJson());

class ModelHighlightUser {
  ModelHighlightUser({
    required this.status,
    required this.data,
  });

  int status;
  List<Datum> data;

  factory ModelHighlightUser.fromJson(Map<String, dynamic> json) => ModelHighlightUser(
    status: json["status"],
    data: List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "status": status,
    "data": List<dynamic>.from(data.map((x) => x.toJson())),
  };
}

class Datum {
  Datum({
    required this.id,
    required this.name,
    required this.email,
    required this.phone,
    required this.birthDate,
    required this.image,
    required this.highlight,
    required this.gender,
    required this.approvement,
    required this.emailVerifiedAt,
    required this.createdAt,
    required this.updatedAt,
  });

  int id;
  String name;
  String email;
  String phone;
  String birthDate;
  String image;
  String highlight;
  String gender;
  String approvement;
  String emailVerifiedAt;
  String createdAt;
  String updatedAt;

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
    id: json["id"],
    name: json["name"],
    email: json["email"],
    phone: json["phone"],
    birthDate: json["birth_date"],
    image: json["image"],
    highlight: json["highlight"],
    gender: json["gender"] == null ? null : json["gender"],
    approvement: json["approvement"],
    emailVerifiedAt: json["email_verified_at"] == null ? null : json["email_verified_at"],
    createdAt: json["created_at"],
    updatedAt: json["updated_at"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "name": name,
    "email": email,
    "phone": phone,
    "birth_date": birthDate,
    "image": image,
    "highlight": highlight,
    "gender": gender == null ? null : gender,
    "approvement": approvement,
    "email_verified_at": emailVerifiedAt == null ? null : emailVerifiedAt,
    "created_at": createdAt,
    "updated_at": updatedAt,
  };
}
