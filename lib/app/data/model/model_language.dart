// To parse this JSON data, do
//
//     final modelLanguage = modelLanguageFromJson(jsonString);

import 'package:meta/meta.dart';
import 'dart:convert';

ModelLanguage modelLanguageFromJson(String str) => ModelLanguage.fromJson(json.decode(str));

String modelLanguageToJson(ModelLanguage data) => json.encode(data.toJson());

class ModelLanguage {
  ModelLanguage({
    required this.languageName,
    required this.proficiency,
  });

  String languageName;
  String proficiency;

  factory ModelLanguage.fromJson(Map<String, dynamic> json) => ModelLanguage(
    languageName: json["language_name"],
    proficiency: json["proficiency"],
  );

  Map<String, dynamic> toJson() => {
    "language_name": languageName,
    "proficiency": proficiency,
  };
}
