// To parse this JSON data, do
//
//     final modelQuiz = modelQuizFromJson(jsonString);

import 'package:meta/meta.dart';
import 'dart:convert';

List<ModelQuiz> modelQuizFromJson(String str) => List<ModelQuiz>.from(json.decode(str).map((x) => ModelQuiz.fromJson(x)));

String modelQuizToJson(List<ModelQuiz> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class ModelQuiz {
  ModelQuiz({
    required this.id,
    required this.categoryId,
    required this.question,
    required this.option1,
    required this.option2,
    required this.option3,
    required this.option4,
    required this.correctAnswer,
    required this.createdAt,
    required this.updatedAt,
  });

  int id;
  String categoryId;
  String question;
  String option1;
  String option2;
  String option3;
  String option4;
  String correctAnswer;
  dynamic createdAt;
  dynamic updatedAt;

  factory ModelQuiz.fromJson(Map<String, dynamic> json) => ModelQuiz(
    id: json["id"],
    categoryId: json["category_id"],
    question: json["question"],
    option1: json["option1"],
    option2: json["option2"],
    option3: json["option3"],
    option4: json["option4"],
    correctAnswer: json["correct_answer"],
    createdAt: json["created_at"],
    updatedAt: json["updated_at"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "category_id": categoryId,
    "question": question,
    "option1": option1,
    "option2": option2,
    "option3": option3,
    "option4": option4,
    "correct_answer": correctAnswer,
    "created_at": createdAt,
    "updated_at": updatedAt,
  };
}
