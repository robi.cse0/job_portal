import 'model_quiz.dart';

class DummyData {

  static String job_description = '''
  - Hands-on experience with CCTV installation & operate, I Phone, Android, Mac Book, Networking is a MUST for this position. 
  
  - Installing, configuring & maintenance computer (Desktop/Laptop) hardware, software, systems, networks, printers, headphones, and other IT appliances.
  
  - Maintain LAN and internet connectivity products and their installation and configuration and troubleshoot, diagnose and resolve computer and network/connectivity problems.
  
  - Troubleshooting system and network problems and diagnosing and solving hardware or software faults;
  
  - Prepare, Install, Configure & Setup and also provide extensive support to Mikrotik, Switch, PC, Laptop, Mac Book, UPS related Hardware and software issues instantly.
  
  - Troubleshoots hardware, software, and networking problems determine the cause of error or stoppage, applies corrective techniques in cases where problems can be corrected, may arrange for repair of faulty equipment or upgrade out-of-date systems.
  
  - Monitors the performance of local area networks tracks significant problems, evaluates usage, and modifies hardware/software for optimal performance.
  
  - English and Bangla Compose, Scanning and Editing and ensure accuracy of typing /data entry work.
  ''';
  static String job_requirements =
  '''
  - Prepare, Install, Configure & Setup and also provide extensive support to Mikrotik, Switch, PC, Laptop, Mac Book, UPS related Hardware and software issues instantly.
  
  - Troubleshoots hardware, software, and networking problems determine the cause of error or stoppage, applies corrective techniques in cases where problems can be corrected, may arrange for repair of faulty equipment or upgrade out-of-date systems.
  
  - Monitors the performance of local area networks tracks significant problems, evaluates usage, and modifies hardware/software for optimal performance.
  
  - English and Bangla Compose, Scanning and Editing and ensure accuracy of typing /data entry work.
  ''';
  static String job_benefits =
  '''
  - Mobile bill, Tour allowance
  - Lunch Facilities: Full Subsidize
  - Salary Review: Yearly
  - Festival Bonus: 2
  ''';
  static String req_education = '''
  - Bachelor in Engineering (BEngg) in CSE
  
  - Skills Required: Android, Good Computer Skills, Hardworking and network, IT System Management
  ''';
}
