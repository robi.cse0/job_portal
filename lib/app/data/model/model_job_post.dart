// To parse this JSON data, do
//
//     final modelJobPost = modelJobPostFromJson(jsonString);

import 'package:meta/meta.dart';
import 'dart:convert';

List<ModelJobPost> modelJobPostFromJson(String str) => List<ModelJobPost>.from(json.decode(str).map((x) => ModelJobPost.fromJson(x)));

String modelJobPostToJson(List<ModelJobPost> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class ModelJobPost {
  ModelJobPost({
    required this.id,
    required this.companyId,
    required this.titleId,
    required this.categoryId,
    required this.position,
    required this.vacancy,
    required this.deadline,
    required this.salary,
    required this.description,
    required this.nature,
    required this.education,
    required this.experience,
    required this.requirements,
    required this.otherBenefits,
    required this.status,
    required this.createdAt,
    required this.updatedAt,
    required this.category,
    required this.title,
    required this.company,
  });

  int id;
  String companyId;
  String titleId;
  String categoryId;
  String position;
  String vacancy;
  String deadline;
  String salary;
  String description;
  String nature;
  String education;
  String experience;
  String requirements;
  String otherBenefits;
  String status;
  String createdAt;
  String updatedAt;
  Category category;
  Title title;
  Company company;

  factory ModelJobPost.fromJson(Map<String, dynamic> json) => ModelJobPost(
    id: json["id"],
    companyId: json["company_id"],
    titleId: json["title_id"],
    categoryId: json["category_id"],
    position: json["position"],
    vacancy: json["vacancy"],
    deadline: json["deadline"],
    salary: json["salary"],
    description: json["description"],
    nature: json["nature"],
    education: json["education"],
    experience: json["experience"],
    requirements: json["requirements"],
    otherBenefits: json["other_benefits"],
    status: json["status"],
    createdAt: json["created_at"],
    updatedAt: json["updated_at"],
    category: Category.fromJson(json["category"]),
    title: Title.fromJson(json["title"]),
    company: Company.fromJson(json["company"]),
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "company_id": companyId,
    "title_id": titleId,
    "category_id": categoryId,
    "position": position,
    "vacancy": vacancy,
    "deadline": deadline,
    "salary": salary,
    "description": description,
    "nature": nature,
    "education": education,
    "experience": experience,
    "requirements": requirements,
    "other_benefits": otherBenefits,
    "status": status,
    "created_at": createdAt,
    "updated_at": updatedAt,
    "category": category.toJson(),
    "title": title.toJson(),
    "company": company.toJson(),
  };
}

class Category {
  Category({
    required this.id,
    required this.category,
    required this.createdAt,
    required this.updatedAt,
  });

  int id;
  String category;
  String createdAt;
  String updatedAt;

  factory Category.fromJson(Map<String, dynamic> json) => Category(
    id: json["id"],
    category: json["category"],
    createdAt: json["created_at"],
    updatedAt: json["updated_at"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "category": category,
    "created_at": createdAt,
    "updated_at": updatedAt,
  };
}

class Company {
  Company({
    required this.id,
    required this.name,
    required this.email,
    required this.phone,
    required this.locationId,
    required this.password,
    required this.description,
    required this.image,
    required this.approved,
    required this.createdAt,
    required this.updatedAt,
  });

  int id;
  String name;
  String email;
  String phone;
  String locationId;
  String password;
  String description;
  String image;
  String approved;
  String createdAt;
  String updatedAt;

  factory Company.fromJson(Map<String, dynamic> json) => Company(
    id: json["id"],
    name: json["name"],
    email: json["email"],
    phone: json["phone"],
    locationId: json["location_id"],
    password: json["password"],
    description: json["description"],
    image: json["image"],
    approved: json["approved"],
    createdAt: json["created_at"],
    updatedAt: json["updated_at"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "name": name,
    "email": email,
    "phone": phone,
    "location_id": locationId,
    "password": password,
    "description": description,
    "image": image,
    "approved": approved,
    "created_at": createdAt,
    "updated_at": updatedAt,
  };
}

class Title {
  Title({
    required this.id,
    required this.title,
    required this.createdAt,
    required this.updatedAt,
  });

  int id;
  String title;
  String createdAt;
  String updatedAt;

  factory Title.fromJson(Map<String, dynamic> json) => Title(
    id: json["id"],
    title: json["title"],
    createdAt: json["created_at"],
    updatedAt: json["updated_at"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "title": title,
    "created_at": createdAt,
    "updated_at": updatedAt,
  };
}
