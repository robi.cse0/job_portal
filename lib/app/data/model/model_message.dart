class Messages{
  String? created_at;
  String? message;
  String? sent_by;

  Messages(this.created_at, this.message, this.sent_by);
}