// To parse this JSON data, do
//
//     final modelEducation = modelEducationFromJson(jsonString);

import 'package:meta/meta.dart';
import 'dart:convert';

ModelEducation modelEducationFromJson(String str) => ModelEducation.fromJson(json.decode(str));

String modelEducationToJson(ModelEducation data) => json.encode(data.toJson());

class ModelEducation {
  ModelEducation({
    required this.level,
    required this.instituteName,
    required this.year,
    required this.gpaCgpa,
  });

  String level;
  String instituteName;
  String year;
  String gpaCgpa;

  factory ModelEducation.fromJson(Map<String, dynamic> json) => ModelEducation(
    level: json["level"],
    instituteName: json["institute_name"],
    year: json["year"],
    gpaCgpa: json["gpa_cgpa"],
  );

  Map<String, dynamic> toJson() => {
    "level": level,
    "institute_name": instituteName,
    "year": year,
    "gpa_cgpa": gpaCgpa,
  };
}
