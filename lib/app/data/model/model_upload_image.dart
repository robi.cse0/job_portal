// To parse this JSON data, do
//
//     final modelUploadImage = modelUploadImageFromJson(jsonString);

import 'package:meta/meta.dart';
import 'dart:convert';

ModelUploadImage modelUploadImageFromJson(String str) => ModelUploadImage.fromJson(json.decode(str));

String modelUploadImageToJson(ModelUploadImage data) => json.encode(data.toJson());

class ModelUploadImage {
  ModelUploadImage({
    required this.status,
    required this.data,
  });

  int status;
  Data data;

  factory ModelUploadImage.fromJson(Map<String, dynamic> json) => ModelUploadImage(
    status: json["status"],
    data: Data.fromJson(json["data"]),
  );

  Map<String, dynamic> toJson() => {
    "status": status,
    "data": data.toJson(),
  };
}

class Data {
  Data({
    required this.id,
    required this.name,
    required this.email,
    required this.phone,
    required this.birthDate,
    required this.image,
    required this.gender,
    required this.approvement,
    required this.emailVerifiedAt,
    required this.createdAt,
    required this.updatedAt,
  });

  int id;
  String name;
  String email;
  String phone;
  DateTime birthDate;
  String image;
  dynamic gender;
  String approvement;
  dynamic emailVerifiedAt;
  DateTime createdAt;
  DateTime updatedAt;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
    id: json["id"],
    name: json["name"],
    email: json["email"],
    phone: json["phone"],
    birthDate: DateTime.parse(json["birth_date"]),
    image: json["image"],
    gender: json["gender"],
    approvement: json["approvement"],
    emailVerifiedAt: json["email_verified_at"],
    createdAt: DateTime.parse(json["created_at"]),
    updatedAt: DateTime.parse(json["updated_at"]),
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "name": name,
    "email": email,
    "phone": phone,
    "birth_date": birthDate.toIso8601String(),
    "image": image,
    "gender": gender,
    "approvement": approvement,
    "email_verified_at": emailVerifiedAt,
    "created_at": createdAt.toIso8601String(),
    "updated_at": updatedAt.toIso8601String(),
  };
}
