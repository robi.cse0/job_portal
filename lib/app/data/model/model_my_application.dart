// To parse this JSON data, do
//
//     final modelMyApplication = modelMyApplicationFromJson(jsonString);

import 'package:meta/meta.dart';
import 'dart:convert';

ModelMyApplication modelMyApplicationFromJson(String str) => ModelMyApplication.fromJson(json.decode(str));

String modelMyApplicationToJson(ModelMyApplication data) => json.encode(data.toJson());

class ModelMyApplication {
  ModelMyApplication({
    required this.applications,
  });

  List<Application> applications;

  factory ModelMyApplication.fromJson(Map<String, dynamic> json) => ModelMyApplication(
    applications: List<Application>.from(json["applications"].map((x) => Application.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "applications": List<dynamic>.from(applications.map((x) => x.toJson())),
  };
}

class Application {
  Application({
    required this.id,
    required this.userId,
    required this.jobId,
    required this.createdAt,
    required this.updatedAt,
    required this.job,
  });

  int id;
  String userId;
  String jobId;
  DateTime createdAt;
  DateTime updatedAt;
  Job job;

  factory Application.fromJson(Map<String, dynamic> json) => Application(
    id: json["id"],
    userId: json["user_id"],
    jobId: json["job_id"],
    createdAt: DateTime.parse(json["created_at"]),
    updatedAt: DateTime.parse(json["updated_at"]),
    job: Job.fromJson(json["job"]),
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "user_id": userId,
    "job_id": jobId,
    "created_at": createdAt.toIso8601String(),
    "updated_at": updatedAt.toIso8601String(),
    "job": job.toJson(),
  };
}

class Job {
  Job({
    required this.id,
    required this.companyId,
    required this.titleId,
    required this.categoryId,
    required this.position,
    required this.vacancy,
    required this.deadline,
    required this.salary,
    required this.description,
    required this.nature,
    required this.education,
    required this.experience,
    required this.requirements,
    required this.otherBenefits,
    required this.status,
    required this.createdAt,
    required this.updatedAt,
    required this.company,
    required this.title,
    required this.category,
  });

  int id;
  String companyId;
  String titleId;
  String categoryId;
  String position;
  String vacancy;
  DateTime deadline;
  String salary;
  String description;
  String nature;
  String education;
  String experience;
  String requirements;
  String otherBenefits;
  String status;
  DateTime createdAt;
  DateTime updatedAt;
  Company company;
  Category title;
  Category category;

  factory Job.fromJson(Map<String, dynamic> json) => Job(
    id: json["id"],
    companyId: json["company_id"],
    titleId: json["title_id"],
    categoryId: json["category_id"],
    position: json["position"],
    vacancy: json["vacancy"],
    deadline: DateTime.parse(json["deadline"]),
    salary: json["salary"],
    description: json["description"],
    nature: json["nature"],
    education: json["education"],
    experience: json["experience"],
    requirements: json["requirements"],
    otherBenefits: json["other_benefits"],
    status: json["status"],
    createdAt: DateTime.parse(json["created_at"]),
    updatedAt: DateTime.parse(json["updated_at"]),
    company: Company.fromJson(json["company"]),
    title: Category.fromJson(json["title"]),
    category: Category.fromJson(json["category"]),
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "company_id": companyId,
    "title_id": titleId,
    "category_id": categoryId,
    "position": position,
    "vacancy": vacancy,
    "deadline": "${deadline.year.toString().padLeft(4, '0')}-${deadline.month.toString().padLeft(2, '0')}-${deadline.day.toString().padLeft(2, '0')}",
    "salary": salary,
    "description": description,
    "nature": nature,
    "education": education,
    "experience": experience,
    "requirements": requirements,
    "other_benefits": otherBenefits,
    "status": status,
    "created_at": createdAt.toIso8601String(),
    "updated_at": updatedAt.toIso8601String(),
    "company": company.toJson(),
    "title": title.toJson(),
    "category": category.toJson(),
  };
}

class Category {
  Category({
    required this.id,
    required this.category,
    required this.createdAt,
    required this.updatedAt,
    required this.title,
  });

  int id;
  String category;
  DateTime createdAt;
  DateTime updatedAt;
  String title;

  factory Category.fromJson(Map<String, dynamic> json) => Category(
    id: json["id"],
    category: json["category"] == null ? null : json["category"],
    createdAt: DateTime.parse(json["created_at"]),
    updatedAt: DateTime.parse(json["updated_at"]),
    title: json["title"] == null ? null : json["title"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "category": category == null ? null : category,
    "created_at": createdAt.toIso8601String(),
    "updated_at": updatedAt.toIso8601String(),
    "title": title == null ? null : title,
  };
}

class Company {
  Company({
    required this.id,
    required this.name,
    required this.email,
    required this.phone,
    required this.locationId,
    required this.password,
    required this.description,
    required this.image,
    required this.approved,
    required this.createdAt,
    required this.updatedAt,
  });

  int id;
  String name;
  String email;
  String phone;
  String locationId;
  String password;
  String description;
  String image;
  String approved;
  DateTime createdAt;
  DateTime updatedAt;

  factory Company.fromJson(Map<String, dynamic> json) => Company(
    id: json["id"],
    name: json["name"],
    email: json["email"],
    phone: json["phone"],
    locationId: json["location_id"],
    password: json["password"],
    description: json["description"],
    image: json["image"],
    approved: json["approved"],
    createdAt: DateTime.parse(json["created_at"]),
    updatedAt: DateTime.parse(json["updated_at"]),
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "name": name,
    "email": email,
    "phone": phone,
    "location_id": locationId,
    "password": password,
    "description": description,
    "image": image,
    "approved": approved,
    "created_at": createdAt.toIso8601String(),
    "updated_at": updatedAt.toIso8601String(),
  };
}
