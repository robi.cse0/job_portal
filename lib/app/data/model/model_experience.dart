// To parse this JSON data, do
//
//     final modelExperience = modelExperienceFromJson(jsonString);

import 'package:meta/meta.dart';
import 'dart:convert';

ModelExperience modelExperienceFromJson(String str) => ModelExperience.fromJson(json.decode(str));

String modelExperienceToJson(ModelExperience data) => json.encode(data.toJson());

class ModelExperience {
  ModelExperience({
    required this.name,
    required this.position,
    required this.period,
    required this.project,
  });

  String name;
  String position;
  String period;
  String project;

  factory ModelExperience.fromJson(Map<String, dynamic> json) => ModelExperience(
    name: json["name"],
    position: json["position"],
    period: json["period"],
    project: json["project"],
  );

  Map<String, dynamic> toJson() => {
    "name": name,
    "position": position,
    "period": period,
    "project": project,
  };
}
