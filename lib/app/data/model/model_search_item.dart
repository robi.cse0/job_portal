// To parse this JSON data, do
//
//     final modelSearchItems = modelSearchItemsFromJson(jsonString);

import 'package:meta/meta.dart';
import 'dart:convert';

ModelSearchItems modelSearchItemsFromJson(String str) => ModelSearchItems.fromJson(json.decode(str));

String modelSearchItemsToJson(ModelSearchItems data) => json.encode(data.toJson());

class ModelSearchItems {
  ModelSearchItems({
    required this.location,
    required this.categories,
    required this.titles,
  });

  List<Location> location;
  List<Category> categories;
  List<Title> titles;

  factory ModelSearchItems.fromJson(Map<String, dynamic> json) => ModelSearchItems(
    location: List<Location>.from(json["location"].map((x) => Location.fromJson(x))),
    categories: List<Category>.from(json["categories"].map((x) => Category.fromJson(x))),
    titles: List<Title>.from(json["titles"].map((x) => Title.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "location": List<dynamic>.from(location.map((x) => x.toJson())),
    "categories": List<dynamic>.from(categories.map((x) => x.toJson())),
    "titles": List<dynamic>.from(titles.map((x) => x.toJson())),
  };
}

class Category {
  Category({
    required this.id,
    required this.category,
    required this.createdAt,
    required this.updatedAt,
  });

  int id;
  String category;
  String createdAt;
  String updatedAt;

  factory Category.fromJson(Map<String, dynamic> json) => Category(
    id: json["id"],
    category: json["category"],
    createdAt: json["created_at"],
    updatedAt: json["updated_at"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "category": category,
    "created_at": createdAt,
    "updated_at": updatedAt,
  };
}

class Location {
  Location({
    required this.id,
    required this.name,
    required this.createdAt,
    required this.updatedAt,
  });

  int id;
  String name;
  String createdAt;
  String updatedAt;

  factory Location.fromJson(Map<String, dynamic> json) => Location(
    id: json["id"],
    name: json["name"],
    createdAt: json["created_at"],
    updatedAt: json["updated_at"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "name": name,
    "created_at": createdAt,
    "updated_at": updatedAt,
  };
}

class Title {
  Title({
    required this.id,
    required this.title,
    required this.createdAt,
    required this.updatedAt,
  });

  int id;
  String title;
  String createdAt;
  String updatedAt;

  factory Title.fromJson(Map<String, dynamic> json) => Title(
    id: json["id"],
    title: json["title"],
    createdAt: json["created_at"],
    updatedAt: json["updated_at"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "title": title,
    "created_at": createdAt,
    "updated_at": updatedAt,
  };
}
