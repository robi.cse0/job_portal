// To parse this JSON data, do
//
//     final modelRegistration = modelRegistrationFromJson(jsonString);

import 'package:meta/meta.dart';
import 'dart:convert';

ModelAuth modelRegistrationFromJson(String str) => ModelAuth.fromJson(json.decode(str));

String modelRegistrationToJson(ModelAuth data) => json.encode(data.toJson());

class ModelAuth {
  ModelAuth({
    required this.status,
    required this.data,
  });

  int status;
  Data data;

  factory ModelAuth.fromJson(Map<String, dynamic> json) => ModelAuth(
    status: json["status"],
    data: Data.fromJson(json["data"]),
  );

  Map<String, dynamic> toJson() => {
    "status": status,
    "data": data.toJson(),
  };
}

class Data {
  Data({
    required this.name,
    required this.email,
    required this.phone,
    required this.birthDate,
    required this.image,
    required this.updatedAt,
    required this.createdAt,
    required this.id,
  });

  String name;
  String email;
  String phone;
  String birthDate;
  String image;
  String updatedAt;
  String createdAt;
  int id;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
    name: json["name"],
    email: json["email"],
    phone: json["phone"],
    image: json["image"] == null ? "" : json["image"],
    birthDate: json["birth_date"],
    updatedAt: json["updated_at"],
    createdAt: json["created_at"],
    id: json["id"],
  );

  Map<String, dynamic> toJson() => {
    "name": name,
    "email": email,
    "phone": phone,
    "image": image,
    "birth_date": birthDate,
    "updated_at": updatedAt,
    "created_at": createdAt,
    "id": id,
  };
}
