// To parse this JSON data, do
//
//     final modelCv = modelCvFromJson(jsonString);

import 'package:meta/meta.dart';
import 'dart:convert';

List<ModelCv> modelCvFromJson(String str) => List<ModelCv>.from(json.decode(str).map((x) => ModelCv.fromJson(x)));

String modelCvToJson(List<ModelCv> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class ModelCv {
  ModelCv({
    required this.id,
    required this.userId,
    required this.name,
    required this.email,
    required this.phone,
    required this.address,
    required this.careerObjective,
    required this.skills,
    required this.experience,
    required this.achievements,
    required this.education,
    required this.languageProficiency,
    required this.createdAt,
    required this.updatedAt,
  });

  int id;
  String userId;
  String name;
  String email;
  String phone;
  String address;
  String careerObjective;
  String skills;
  String experience;
  String achievements;
  String education;
  String languageProficiency;
  DateTime createdAt;
  DateTime updatedAt;

  factory ModelCv.fromJson(Map<String, dynamic> json) => ModelCv(
    id: json["id"],
    userId: json["user_id"],
    name: json["name"],
    email: json["email"],
    phone: json["phone"],
    address: json["address"],
    careerObjective: json["career_objective"],
    skills: json["skills"],
    experience: json["experience"],
    achievements: json["achievements"],
    education: json["education"],
    languageProficiency: json["language_proficiency"],
    createdAt: DateTime.parse(json["created_at"]),
    updatedAt: DateTime.parse(json["updated_at"]),
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "user_id": userId,
    "name": name,
    "email": email,
    "phone": phone,
    "address": address,
    "career_objective": careerObjective,
    "skills": skills,
    "experience": experience,
    "achievements": achievements,
    "education": education,
    "language_proficiency": languageProficiency,
    "created_at": createdAt.toIso8601String(),
    "updated_at": updatedAt.toIso8601String(),
  };
}
