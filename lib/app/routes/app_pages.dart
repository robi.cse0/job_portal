import 'package:get/get.dart';

import '../modules/cv_maker/bindings/cv_maker_binding.dart';
import '../modules/cv_maker/views/cv_maker_view.dart';
import '../modules/cv_preview/bindings/cv_preview_binding.dart';
import '../modules/cv_preview/views/cv_preview_view.dart';
import '../modules/help_line/bindings/help_line_binding.dart';
import '../modules/help_line/views/help_line_view.dart';
import '../modules/home/bindings/home_binding.dart';
import '../modules/home/views/home_view.dart';
import '../modules/hot_jobs/bindings/hot_jobs_binding.dart';
import '../modules/hot_jobs/views/hot_jobs_view.dart';
import '../modules/job_details/bindings/job_details_binding.dart';
import '../modules/job_details/views/job_details_view.dart';
import '../modules/login/bindings/login_binding.dart';
import '../modules/login/views/login_view.dart';
import '../modules/my_jobs/bindings/my_jobs_binding.dart';
import '../modules/my_jobs/views/my_jobs_view.dart';
import '../modules/profile/bindings/profile_binding.dart';
import '../modules/profile/views/profile_view.dart';
import '../modules/profile_highlights/bindings/profile_highlights_binding.dart';
import '../modules/profile_highlights/views/profile_highlights_view.dart';
import '../modules/quiz/bindings/quiz_binding.dart';
import '../modules/quiz/views/quiz_view.dart';
import '../modules/registration_user/bindings/registration_user_binding.dart';
import '../modules/registration_user/views/registration_user_view.dart';
import '../modules/search/bindings/search_binding.dart';
import '../modules/search/views/search_view.dart';
import '../modules/search_result/bindings/search_result_binding.dart';
import '../modules/search_result/views/search_result_view.dart';
import '../modules/splash/bindings/splash_binding.dart';
import '../modules/splash/views/splash_view.dart';
import '../modules/update_profile/bindings/update_profile_binding.dart';
import '../modules/update_profile/views/update_profile_view.dart';
import '../modules/update_profile_picture/bindings/update_profile_picture_binding.dart';
import '../modules/update_profile_picture/views/update_profile_picture_view.dart';

part 'app_routes.dart';

class AppPages {
  AppPages._();

  static const INITIAL = Routes.SPLASH;

  static final routes = [
    GetPage(
      name: _Paths.HOME,
      page: () => HomeView(),
      binding: HomeBinding(),
    ),
    GetPage(
      name: _Paths.SPLASH,
      page: () => SplashView(),
      binding: SplashBinding(),
    ),
    GetPage(
      name: _Paths.LOGIN,
      page: () => LoginView(),
      binding: LoginBinding(),
    ),
    GetPage(
      name: _Paths.REGISTRATION_USER,
      page: () => RegistrationUserView(),
      binding: RegistrationUserBinding(),
    ),
    GetPage(
      name: _Paths.SEARCH,
      page: () => SearchView(),
      binding: SearchBinding(),
    ),
    GetPage(
      name: _Paths.PROFILE,
      page: () => ProfileView(),
      binding: ProfileBinding(),
    ),
    GetPage(
      name: _Paths.HOT_JOBS,
      page: () => HotJobsView(),
      binding: HotJobsBinding(),
    ),
    GetPage(
      name: _Paths.CV_MAKER,
      page: () => CvMakerView(),
      binding: CvMakerBinding(),
    ),
    GetPage(
      name: _Paths.JOB_DETAILS,
      page: () => JobDetailsView(),
      binding: JobDetailsBinding(),
    ),
    GetPage(
      name: _Paths.QUIZ,
      page: () => QuizView(),
      binding: QuizBinding(),
    ),
    GetPage(
      name: _Paths.SEARCH_RESULT,
      page: () => SearchResultView(),
      binding: SearchResultBinding(),
    ),
    GetPage(
      name: _Paths.CV_PREVIEW,
      page: () => CvPreviewView(),
      binding: CvPreviewBinding(),
    ),
    GetPage(
      name: _Paths.UPDATE_PROFILE,
      page: () => UpdateProfileView(),
      binding: UpdateProfileBinding(),
    ),
    GetPage(
      name: _Paths.UPDATE_PROFILE_PICTURE,
      page: () => UpdateProfilePictureView(),
      binding: UpdateProfilePictureBinding(),
    ),
    GetPage(
      name: _Paths.MY_JOBS,
      page: () => MyJobsView(),
      binding: MyJobsBinding(),
    ),
    GetPage(
      name: _Paths.HELP_LINE,
      page: () => HelpLineView(),
      binding: HelpLineBinding(),
    ),
    GetPage(
      name: _Paths.PROFILE_HIGHLIGHTS,
      page: () => ProfileHighlightsView(),
      binding: ProfileHighlightsBinding(),
    ),
  ];
}
