part of 'app_pages.dart';
// DO NOT EDIT. This is code generated via package:get_cli/get_cli.dart

abstract class Routes {
  Routes._();

  static const HOME = _Paths.HOME;
  static const SPLASH = _Paths.SPLASH;
  static const LOGIN = _Paths.LOGIN;
  static const REGISTRATION_USER = _Paths.REGISTRATION_USER;
  static const SEARCH = _Paths.SEARCH;
  static const PROFILE = _Paths.PROFILE;
  static const HOT_JOBS = _Paths.HOT_JOBS;
  static const CV_MAKER = _Paths.CV_MAKER;
  static const JOB_DETAILS = _Paths.JOB_DETAILS;
  static const QUIZ = _Paths.QUIZ;
  static const SEARCH_RESULT = _Paths.SEARCH_RESULT;
  static const CV_PREVIEW = _Paths.CV_PREVIEW;
  static const UPDATE_PROFILE = _Paths.UPDATE_PROFILE;
  static const UPDATE_PROFILE_PICTURE = _Paths.UPDATE_PROFILE_PICTURE;
  static const MY_JOBS = _Paths.MY_JOBS;
  static const HELP_LINE = _Paths.HELP_LINE;
  static const PROFILE_HIGHLIGHTS = _Paths.PROFILE_HIGHLIGHTS;
}

abstract class _Paths {
  static const HOME = '/home';
  static const SPLASH = '/splash';
  static const LOGIN = '/login';
  static const REGISTRATION_USER = '/registration-user';
  static const SEARCH = '/search';
  static const PROFILE = '/profile';
  static const HOT_JOBS = '/hot-jobs';
  static const CV_MAKER = '/cv-maker';
  static const JOB_DETAILS = '/job-details';
  static const QUIZ = '/quiz';
  static const SEARCH_RESULT = '/search-result';
  static const CV_PREVIEW = '/cv-preview';
  static const UPDATE_PROFILE = '/update-profile';
  static const UPDATE_PROFILE_PICTURE = '/update-profile-picture';
  static const MY_JOBS = '/my-jobs';
  static const HELP_LINE = '/help-line';
  static const PROFILE_HIGHLIGHTS = '/profile-highlights';
}
