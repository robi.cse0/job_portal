class PrefKeys {
  static String LOGIN_STATUS = "LOGIN_STATUS";


  //CV
  static String CV_NAME = "CV_NAME";
  static String CV_EMAIL = "CV_EMAIL";
  static String CV_ADDRESS = "CV_ADDRESS";
  static String CV_MOBILE = "CV_MOBILE";
  static String CV_OBJECTIVE = "CV_OBJECTIVE";
  static String CV_SKILLS = "CV_SKILLS";
  static String CV_EXPERIENCE = "CV_EXPERIENCE";
  static String CV_ACHEIVEMENTS = "CV_ACHEIVEMENTS";
  static String CV_EDUCATION = "CV_EDUCATION";
  static String CV_LANGUAGE = "CV_LANGUAGE";

  //Profile
  static String USER_NAME = "USER_NAME";
  static String USER_EMAIL = "USER_EMAIL";
  static String USER_ID = "USER_ID";
  static String USER_IMAGE = "USER_IMAGE";
  static String USER_MOBILE = "USER_MOBILE";
  static String USER_DOB = "USER_DOB";



}
