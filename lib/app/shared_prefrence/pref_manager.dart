import 'package:get_storage/get_storage.dart';

class PrefManager {
  //Store integer value
  static putValue(String key, dynamic value) {
    GetStorage().write(key, value);
  }

  static putString(String key, String value) {
    GetStorage().write(key, value);
  }

  static putList(String key, List<String> value) {
    GetStorage().write(key, value);
  }

  static getList(String key) {
    GetStorage().read(key);
  }

  //Return Integer value
  static getInt(String key, {int default_value = 0}) {
    if (GetStorage().read(key) != null) {
      return GetStorage().read(key);
    } else {
      return default_value;
    }
  }

  //Return String value
  static getString(String key, {String default_value = ""}) {
    if (GetStorage().read(key) != null) {
      return GetStorage().read(key);
    } else {
      return default_value;
    }
  }

  //Return List

  //Return Boolean value
  static getBoolean(String key, {bool default_value = false}) {
    if (GetStorage().read(key) != null) {
      return GetStorage().read(key);
    } else {
      return default_value;
    }
  }

  static clear() {
    GetStorage().erase();
  }
}
