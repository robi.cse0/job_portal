import 'package:flutter/material.dart';
import 'package:get/get.dart';

class SnackBarWidget {

  static showMessage(String msg) {
    Get.rawSnackbar(
        message: msg,
        backgroundColor: Colors.black,
        snackPosition: SnackPosition.TOP,
        borderRadius: 8,
        margin: EdgeInsets.only(left: 10, right: 10, top: 20)
    );
  }

}