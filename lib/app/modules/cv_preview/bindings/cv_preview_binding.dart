import 'package:get/get.dart';

import '../controllers/cv_preview_controller.dart';

class CvPreviewBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<CvPreviewController>(
      () => CvPreviewController(),
    );
  }
}
