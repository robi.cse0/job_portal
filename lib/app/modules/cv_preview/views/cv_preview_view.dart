import 'dart:convert';

import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:job_portal/app/routes/app_pages.dart';
import 'package:job_portal/app/shared_prefrence/pref_keys.dart';
import 'package:job_portal/app/shared_prefrence/pref_manager.dart';
import 'package:job_portal/app/utils/app_constant.dart';
import 'package:job_portal/app/utils/loading_bar.dart';
import 'package:percent_indicator/linear_percent_indicator.dart';
import 'package:screenshot/screenshot.dart';

import '../controllers/cv_preview_controller.dart';

const h1 = TextStyle(fontSize: 18, fontWeight: FontWeight.bold);
const h2 = TextStyle(fontSize: 17, fontWeight: FontWeight.bold);
const h3 = TextStyle(fontSize: 15, fontWeight: FontWeight.bold);
const normal_font = TextStyle(fontSize: 14, fontWeight: FontWeight.normal);

class CvPreviewView extends GetView<CvPreviewController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text('My CV'),
        centerTitle: true,
        actions: [
          Center(
            child: InkWell(
              onTap: () {
                controller.createPdf();
              },
              child: Container(
                  margin: EdgeInsets.only(right: 15),
                  child: Text(
                    "Download",
                    style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold),
                  )),
            ),
          ),
          Center(
            child: InkWell(
              onTap: () {
                Get.toNamed(Routes.CV_MAKER);
              },
              child: Container(
                  margin: EdgeInsets.only(right: 15),
                  child: Text(
                    "Edit",
                    style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold),
                  )),
            ),
          ),
        ],
      ),
      body: Obx(() {
        return controller.isLoading.isFalse
            ? SingleChildScrollView(
                child: Container(
                  padding: EdgeInsets.all(20),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        child: Text(
                          "Your CV Progress :",
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: 18,
                              fontWeight: FontWeight.bold),
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.fromLTRB(0, 20, 0, 20),
                        child: LinearPercentIndicator(
                          lineHeight: 18.0,
                          percent: AppConstant.cv_progress.value / 100,
                          center: Text(
                            "${AppConstant.cv_progress.value}%",
                            style: new TextStyle(fontSize: 12.0),
                          ),
                          trailing: Image.asset(
                            AppConstant.cv_progress.value > 80
                                ? "assets/images/green_click.png"
                                : "assets/images/ic_info_red.png",
                            height: 25,
                            width: 25,
                          ),
                          linearStrokeCap: LinearStrokeCap.roundAll,
                          backgroundColor: Colors.grey,
                          progressColor: Colors.blue,
                        ),
                      ),
                      Screenshot(
                        controller: controller.screenshotController,
                        child: Container(
                          color: Colors.white,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Expanded(
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        //Name
                                        Container(
                                          margin: EdgeInsets.only(bottom: 10),
                                          child: Text(
                                            PrefManager.getString(
                                                PrefKeys.CV_NAME,
                                                default_value: "No Name"),
                                            style: h1,
                                          ),
                                        ),

                                        //Email
                                        Container(
                                          margin: EdgeInsets.only(bottom: 10),
                                          child: Text(
                                            "Email     : ${PrefManager.getString(PrefKeys.CV_EMAIL, default_value: "No email added")}",
                                            style: normal_font,
                                          ),
                                        ),

                                        //Phone
                                        Container(
                                          margin: EdgeInsets.only(bottom: 10),
                                          child: Text(
                                            "Phone    : ${PrefManager.getString(PrefKeys.CV_MOBILE, default_value: "No Phone number found")}",
                                            style: normal_font,
                                          ),
                                        ),

                                        //Address
                                        Container(
                                          margin: EdgeInsets.only(bottom: 10),
                                          child: Text(
                                            "Address : ${PrefManager.getString(PrefKeys.CV_ADDRESS, default_value: "No address found")}",
                                            style: normal_font,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  SizedBox(
                                    height: 100.0,
                                    width: 100.0,
                                    child: ClipRRect(
                                      borderRadius: BorderRadius.circular(50.0),
                                      child: Image.network(
                                        PrefManager.getString(
                                                    PrefKeys.USER_IMAGE) !=
                                                ""
                                            ? "https://mazharsany.xyz/jobs-today/storage/app/" +
                                                PrefManager.getString(
                                                    PrefKeys.USER_IMAGE)
                                            : "https://cdn.pixabay.com/photo/2016/08/08/09/17/avatar-1577909_960_720.png",
                                        height: 100.0,
                                        width: 100.0,
                                        fit: BoxFit.cover,
                                      ),
                                    ),
                                  )
                                ],
                              ),

                              //Career Objective
                              Container(
                                color: Colors.grey[300],
                                width: Get.width,
                                margin: EdgeInsets.only(top: 20, bottom: 10),
                                padding: EdgeInsets.only(
                                    left: 10, top: 5, bottom: 5),
                                child: Text(
                                  "Career Objective",
                                  style: h2,
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.only(bottom: 10, left: 10),
                                child: Text(
                                  "${PrefManager.getString(PrefKeys.CV_OBJECTIVE) != "" ? PrefManager.getString(PrefKeys.CV_OBJECTIVE) : "No data available"}",
                                  style: normal_font,
                                ),
                              ),

                              //Skills
                              Container(
                                color: Colors.grey[300],
                                width: Get.width,
                                margin: EdgeInsets.only(top: 20, bottom: 10),
                                padding: EdgeInsets.only(
                                    left: 10, top: 5, bottom: 5),
                                child: Text(
                                  "Skills",
                                  style: h2,
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.only(left: 10, bottom: 15),
                                child: controller.skillsList.length > 0
                                    ? ListView.builder(
                                        physics: NeverScrollableScrollPhysics(),
                                        shrinkWrap: true,
                                        itemCount: controller.skillsList.length,
                                        itemBuilder: (context, index) {
                                          return Container(
                                            child: Row(
                                              children: [
                                                Container(
                                                  height: 8,
                                                  width: 8,
                                                  decoration: BoxDecoration(
                                                    borderRadius:
                                                        BorderRadius.all(
                                                      Radius.circular(4.0),
                                                    ),
                                                    color: Colors.grey,
                                                  ),
                                                ),
                                                SizedBox(
                                                  width: 10,
                                                ),
                                                Expanded(
                                                  child: Container(
                                                    child: Text(
                                                      controller
                                                          .skillsList[index],
                                                      style: TextStyle(
                                                          fontSize: 16),
                                                    ),
                                                  ),
                                                )
                                              ],
                                            ),
                                          );
                                        })
                                    : Text("No Data Available"),
                              ),

                              //Professional Experience
                              Container(
                                color: Colors.grey[300],
                                width: Get.width,
                                margin: EdgeInsets.only(bottom: 10),
                                padding: EdgeInsets.only(
                                    left: 10, top: 5, bottom: 5),
                                child: Text(
                                  "Professional Experience",
                                  style: h2,
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.only(left: 10, bottom: 15),
                                child: controller.experienceList.length > 0
                                    ? ListView.builder(
                                        physics: NeverScrollableScrollPhysics(),
                                        shrinkWrap: true,
                                        itemCount:
                                            controller.experienceList.length,
                                        itemBuilder: (context, index) {
                                          return Container(
                                            padding: EdgeInsets.only(bottom: 8),
                                            child: Row(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: [
                                                Container(
                                                  margin:
                                                      EdgeInsets.only(top: 4),
                                                  width: 10,
                                                  height: 10,
                                                  color: Colors.cyan,
                                                ),
                                                SizedBox(
                                                  width: 10,
                                                ),
                                                Expanded(
                                                  child: Column(
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment
                                                            .start,
                                                    children: [
                                                      Container(
                                                        child: Text(
                                                          controller
                                                              .experienceList[
                                                                  index]
                                                              .name,
                                                          style: h3,
                                                        ),
                                                      ),
                                                      SizedBox(
                                                        height: 10,
                                                      ),
                                                      Container(
                                                        child: Text(
                                                          "Position : ${controller.experienceList[index].position}",
                                                          style: TextStyle(
                                                              fontSize: 16,
                                                              fontWeight:
                                                                  FontWeight
                                                                      .normal),
                                                        ),
                                                      ),
                                                      SizedBox(
                                                        height: 5,
                                                      ),
                                                      Container(
                                                        child: Text(
                                                          "Working period : ${controller.experienceList[index].period}",
                                                          style: TextStyle(
                                                              fontSize: 16,
                                                              fontWeight:
                                                                  FontWeight
                                                                      .normal),
                                                        ),
                                                      ),
                                                      SizedBox(
                                                        height: 5,
                                                      ),
                                                      Container(
                                                        child: Text(
                                                          "Job details : ${controller.experienceList[index].project}",
                                                          style: TextStyle(
                                                              fontSize: 16,
                                                              fontWeight:
                                                                  FontWeight
                                                                      .normal),
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                )
                                              ],
                                            ),
                                          );
                                        })
                                    : Text("No Data Available"),
                              ),

                              //Acheivements
                              Container(
                                color: Colors.grey[300],
                                width: Get.width,
                                margin: EdgeInsets.only(bottom: 10),
                                padding: EdgeInsets.only(
                                    left: 10, top: 5, bottom: 5),
                                child: Text(
                                  "Achievements",
                                  style: h2,
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.only(left: 10, bottom: 15),
                                child: controller.achievementList.length > 0
                                    ? ListView.builder(
                                        physics: NeverScrollableScrollPhysics(),
                                        shrinkWrap: true,
                                        itemCount:
                                            controller.achievementList.length,
                                        itemBuilder: (context, index) {
                                          return Container(
                                            padding: EdgeInsets.all(3),
                                            child: Row(
                                              children: [
                                                Container(
                                                  height: 8,
                                                  width: 8,
                                                  decoration: BoxDecoration(
                                                    borderRadius:
                                                        BorderRadius.all(
                                                      Radius.circular(4.0),
                                                    ),
                                                    color: Colors.grey,
                                                  ),
                                                ),
                                                SizedBox(
                                                  width: 10,
                                                ),
                                                Expanded(
                                                  child: Container(
                                                    child: Text(
                                                      controller
                                                              .achievementList[
                                                          index],
                                                      style: TextStyle(
                                                          fontSize: 16),
                                                    ),
                                                  ),
                                                ),
                                              ],
                                            ),
                                          );
                                        })
                                    : Text("No Data Available"),
                              ),

                              //Education
                              Container(
                                color: Colors.grey[300],
                                width: Get.width,
                                margin: EdgeInsets.only(bottom: 10),
                                padding: EdgeInsets.only(
                                    left: 10, top: 5, bottom: 5),
                                child: Text(
                                  "Education",
                                  style: h2,
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.only(left: 0, bottom: 15),
                                child: controller.educationList.length > 0
                                    ? ListView.builder(
                                        physics: NeverScrollableScrollPhysics(),
                                        shrinkWrap: true,
                                        itemCount:
                                            controller.educationList.length,
                                        itemBuilder: (context, index) {
                                          return Container(
                                            padding: EdgeInsets.all(8),
                                            child: Row(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: [
                                                Container(
                                                  margin:
                                                      EdgeInsets.only(top: 4),
                                                  width: 10,
                                                  height: 10,
                                                  color: Colors.cyan,
                                                ),
                                                SizedBox(
                                                  width: 10,
                                                ),
                                                Expanded(
                                                  child: Column(
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment
                                                            .start,
                                                    children: [
                                                      Container(
                                                        child: Text(
                                                          controller
                                                              .educationList[
                                                                  index]
                                                              .level,
                                                          style: TextStyle(
                                                              fontSize: 16,
                                                              fontWeight:
                                                                  FontWeight
                                                                      .bold),
                                                        ),
                                                      ),
                                                      SizedBox(
                                                        height: 10,
                                                      ),
                                                      Container(
                                                        child: Text(
                                                          "Institute : ${controller.educationList[index].instituteName}",
                                                          style: TextStyle(
                                                              fontSize: 16,
                                                              fontWeight:
                                                                  FontWeight
                                                                      .normal),
                                                        ),
                                                      ),
                                                      SizedBox(
                                                        height: 5,
                                                      ),
                                                      Container(
                                                        child: Text(
                                                          "Year : ${controller.educationList[index].year}",
                                                          style: TextStyle(
                                                              fontSize: 16,
                                                              fontWeight:
                                                                  FontWeight
                                                                      .normal),
                                                        ),
                                                      ),
                                                      SizedBox(
                                                        height: 5,
                                                      ),
                                                      Container(
                                                        child: Text(
                                                          "GPA/CGPA : ${controller.educationList[index].gpaCgpa}",
                                                          style: TextStyle(
                                                              fontSize: 16,
                                                              fontWeight:
                                                                  FontWeight
                                                                      .normal),
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                              ],
                                            ),
                                          );
                                        })
                                    : Text("No Data Available"),
                              ),

                              //Language
                              Container(
                                color: Colors.grey[300],
                                width: Get.width,
                                margin: EdgeInsets.only(bottom: 10),
                                padding: EdgeInsets.only(
                                    left: 10, top: 5, bottom: 5),
                                child: Text(
                                  "Language Proficiency",
                                  style: h2,
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.only(bottom: 10, left: 10),
                                child: controller.languageList.length > 0
                                    ? ListView.builder(
                                        physics: NeverScrollableScrollPhysics(),
                                        shrinkWrap: true,
                                        itemCount:
                                            controller.languageList.length,
                                        itemBuilder: (context, index) {
                                          return Container(
                                            padding: EdgeInsets.only(
                                                top: 8, bottom: 8),
                                            child: Row(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: [
                                                Container(
                                                  margin:
                                                      EdgeInsets.only(top: 4),
                                                  width: 10,
                                                  height: 10,
                                                  color: Colors.cyan,
                                                ),
                                                SizedBox(
                                                  width: 10,
                                                ),
                                                Expanded(
                                                  child: Column(
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment
                                                            .start,
                                                    children: [
                                                      Container(
                                                        child: Text(
                                                          controller
                                                              .languageList[
                                                                  index]
                                                              .languageName,
                                                          style: TextStyle(
                                                              fontSize: 17,
                                                              fontWeight:
                                                                  FontWeight
                                                                      .bold),
                                                        ),
                                                      ),
                                                      SizedBox(
                                                        height: 10,
                                                      ),
                                                      Container(
                                                        child: Text(
                                                          "Fluency : ${controller.languageList[index].proficiency}",
                                                          style: TextStyle(
                                                              fontSize: 16,
                                                              fontWeight:
                                                                  FontWeight
                                                                      .normal),
                                                        ),
                                                      ),
                                                      SizedBox(
                                                        height: 5,
                                                      ),
                                                    ],
                                                  ),
                                                )
                                              ],
                                            ),
                                          );
                                        })
                                    : Text("No Data Available"),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              )
            : Container(
                height: Get.height,
                width: Get.width,
                child: LoadingBar.spinkit,
              );
      }),
    );
  }
}
