import 'dart:convert';
import 'dart:io';

import 'package:dio/dio.dart';
import 'package:get/get.dart';
import 'package:job_portal/app/data/model/ModelCV.dart';
import 'package:job_portal/app/data/model/model_education.dart';
import 'package:job_portal/app/data/model/model_experience.dart';
import 'package:job_portal/app/data/model/model_language.dart';
import 'package:job_portal/app/data/network/apis.dart';
import 'package:job_portal/app/data/network/http_service.dart';
import 'package:job_portal/app/shared_prefrence/pref_keys.dart';
import 'package:job_portal/app/shared_prefrence/pref_manager.dart';
import 'package:job_portal/app/utils/app_constant.dart';
import 'package:job_portal/app/utils/snack_bar.dart';
import 'package:open_file/open_file.dart';
import 'package:pdf/pdf.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:screenshot/screenshot.dart';
import 'package:pdf/widgets.dart' as pw;

class CvPreviewController extends GetxController {
  ScreenshotController screenshotController = new ScreenshotController();
  HttpService _httpService = Get.put(HttpService());
  RxBool isLoading = false.obs;

  var skillsList = <String>[].obs;
  var achievementList = <String>[].obs;
  var experienceList = <ModelExperience>[].obs;
  var educationList = <ModelEducation>[].obs;
  var languageList = <ModelLanguage>[].obs;

  @override
  void onInit() {
    super.onInit();
    getCV();
    updateProgress();
  }

  @override
  void onReady() {
    super.onReady();
    skillsList.value = getSkillList();
    experienceList.value = getExperienceList();
    achievementList.value = getAcievementList();
    educationList.value = getEducationList();
    languageList.value = getLanguageList();
  }

  @override
  void onClose() {}

  List<String> getSkillList() {
    String data = PrefManager.getString(PrefKeys.CV_SKILLS);
    if (data == "") {
      PrefManager.putValue(PrefKeys.CV_SKILLS, "[]");
      return <String>[];
    } else {
      return jsonToList(data);
    }
  }

  getAcievementList() {
    String data = PrefManager.getString(PrefKeys.CV_ACHEIVEMENTS);
    if (data == "") {
      PrefManager.putValue(PrefKeys.CV_ACHEIVEMENTS, "[]");
      return <String>[];
    } else {
      return jsonToList(data);
    }
  }

  jsonToList(String data) {
    List<String> finalList = [];
    var list = json.decode(data);
    for (int i = 0; i < list.length; i++) {
      finalList.add(list[i]);
    }
    return finalList;
  }

  getExperienceList() {
    String data = PrefManager.getString(PrefKeys.CV_EXPERIENCE);
    if (data == "") {
      PrefManager.putValue(PrefKeys.CV_EXPERIENCE, "[]");
      return <ModelExperience>[];
    } else {
      return jsonToExperienceList(data);
    }
  }

  jsonToExperienceList(String data) {
    List<ModelExperience> finalList = [];
    var list = json
        .decode(data)
        .map((data) => ModelExperience.fromJson(data))
        .toList();

    for (int i = 0; i < list.length; i++) {
      finalList.add(list[i]);
    }

    return finalList;
  }

  getEducationList() {
    String data = PrefManager.getString(PrefKeys.CV_EDUCATION);
    if (data == "") {
      PrefManager.putValue(PrefKeys.CV_EDUCATION, "[]");
      return <ModelEducation>[];
    } else {
      return jsonToEducationList(data);
    }
  }

  jsonToEducationList(String data) {
    List<ModelEducation> finalList = [];
    var list =
        json.decode(data).map((data) => ModelEducation.fromJson(data)).toList();

    for (int i = 0; i < list.length; i++) {
      finalList.add(list[i]);
    }

    return finalList;
  }

  getLanguageList() {
    String data = PrefManager.getString(PrefKeys.CV_LANGUAGE);
    if (data == "") {
      PrefManager.putValue(PrefKeys.CV_LANGUAGE, "[]");
      return <ModelLanguage>[];
    } else {
      return jsonToLanguageList(data);
    }
  }

  jsonToLanguageList(String data) {
    List<ModelLanguage> finalList = [];
    var list =
        json.decode(data).map((data) => ModelLanguage.fromJson(data)).toList();

    for (int i = 0; i < list.length; i++) {
      finalList.add(list[i]);
    }

    return finalList;
  }

  String imgExtension = '.jpg';
  String fileName = '${PrefManager.getString(PrefKeys.CV_NAME)}_CV';

  static final h1 = pw.TextStyle(fontSize: 18, fontWeight: pw.FontWeight.bold);
  static final h2 = pw.TextStyle(fontSize: 17, fontWeight: pw.FontWeight.bold);
  static final h3 = pw.TextStyle(fontSize: 15, fontWeight: pw.FontWeight.bold);
  static final normal_font =
      pw.TextStyle(fontSize: 14, fontWeight: pw.FontWeight.normal);

  createPdf() async {
    var status = await Permission.storage.status;
    if (!status.isGranted) {
      await Permission.storage.request();
    } else {
      final doc = pw.Document(deflate: zlib.encode);
      doc.addPage(pw.Page(
          pageFormat: PdfPageFormat.a4,
          build: (pw.Context context) {
            return pw.Container(
              padding: pw.EdgeInsets.all(20),
              child: pw.Column(
                crossAxisAlignment: pw.CrossAxisAlignment.start,
                children: [
                  pw.Row(
                    mainAxisAlignment: pw.MainAxisAlignment.spaceBetween,
                    children: [
                      pw.Expanded(
                        child: pw.Column(
                          crossAxisAlignment: pw.CrossAxisAlignment.start,
                          children: [
                            //Name
                            pw.Container(
                              margin: pw.EdgeInsets.only(bottom: 10),
                              child: pw.Text(
                                PrefManager.getString(PrefKeys.CV_NAME),
                                style: h1,
                              ),
                            ),

                            //Email
                            pw.Container(
                              margin: pw.EdgeInsets.only(bottom: 10),
                              child: pw.Text(
                                "Email     : ${PrefManager.getString(PrefKeys.CV_EMAIL)}",
                                style: normal_font,
                              ),
                            ),

                            //Phone
                            pw.Container(
                              margin: pw.EdgeInsets.only(bottom: 10),
                              child: pw.Text(
                                "Phone    : ${PrefManager.getString(PrefKeys.CV_MOBILE)}",
                                style: normal_font,
                              ),
                            ),

                            //Address
                            pw.Container(
                              margin: pw.EdgeInsets.only(bottom: 10),
                              child: pw.Text(
                                "Address : ${PrefManager.getString(PrefKeys.CV_ADDRESS)}",
                                style: normal_font,
                              ),
                            ),
                          ],
                        ),
                      ),
                      // pw.ClipRRect(
                      //   horizontalRadius: 50,
                      //   verticalRadius: 50,
                      //   child: pw.Image(
                      //     PrefManager.getString(PrefKeys.USER_IMAGE),
                      //     height: 100.0,
                      //     width: 100.0,
                      //     fit: pw.BoxFit.cover,
                      //   ),
                      // )
                    ],
                  ),

                  //Career Objective
                  pw.Container(
                    color: PdfColor.fromHex("#D3D3D3"),
                    width: Get.width,
                    margin: pw.EdgeInsets.only(top: 20, bottom: 10),
                    padding: pw.EdgeInsets.only(left: 10, top: 5, bottom: 5),
                    child: pw.Text(
                      "Career Objective",
                      style: h2,
                    ),
                  ),
                  pw.Container(
                    margin: pw.EdgeInsets.only(bottom: 10, left: 10),
                    child: pw.Text(
                      "${PrefManager.getString(PrefKeys.CV_OBJECTIVE)}",
                      style: normal_font,
                    ),
                  ),

                  //Skills
                  pw.Container(
                    color: PdfColor.fromHex("#808080"),
                    width: Get.width,
                    margin: pw.EdgeInsets.only(top: 20, bottom: 10),
                    padding: pw.EdgeInsets.only(left: 10, top: 5, bottom: 5),
                    child: pw.Text(
                      "Skills",
                      style: h2,
                    ),
                  ),
                  pw.Container(
                    margin: pw.EdgeInsets.only(left: 10, bottom: 15),
                    child: pw.ListView.builder(
                        itemCount: skillsList.length,
                        itemBuilder: (context, index) {
                          return pw.Container(
                            child: pw.Row(
                              children: [
                                pw.Container(
                                  height: 8,
                                  width: 8,
                                  decoration: pw.BoxDecoration(
                                    borderRadius: pw.BorderRadius.all(
                                      pw.Radius.circular(4.0),
                                    ),
                                    // color: Colors.grey,
                                  ),
                                ),
                                pw.SizedBox(
                                  width: 10,
                                ),
                                pw.Expanded(
                                  child: pw.Container(
                                    child: pw.Text(
                                      skillsList[index],
                                      style: pw.TextStyle(fontSize: 16),
                                    ),
                                  ),
                                )
                              ],
                            ),
                          );
                        }),
                  ),

                  //Professional Experience
                  pw.Container(
                    color: PdfColor.fromHex("#808080"),
                    width: Get.width,
                    margin: pw.EdgeInsets.only(bottom: 10),
                    padding: pw.EdgeInsets.only(left: 10, top: 5, bottom: 5),
                    child: pw.Text(
                      "Professional Experience",
                      style: h2,
                    ),
                  ),
                  pw.Container(
                    margin: pw.EdgeInsets.only(left: 10, bottom: 15),
                    child: pw.ListView.builder(
                        itemCount: experienceList.length,
                        itemBuilder: (context, index) {
                          return pw.Container(
                            padding: pw.EdgeInsets.only(bottom: 8),
                            child: pw.Row(
                              crossAxisAlignment: pw.CrossAxisAlignment.start,
                              children: [
                                pw.Container(
                                  margin: pw.EdgeInsets.only(top: 4),
                                  width: 10,
                                  height: 10,
                                  color: PdfColor.fromHex("#808080"),
                                ),
                                pw.SizedBox(
                                  width: 10,
                                ),
                                pw.Expanded(
                                  child: pw.Column(
                                    crossAxisAlignment:
                                        pw.CrossAxisAlignment.start,
                                    children: [
                                      pw.Container(
                                        child: pw.Text(
                                          experienceList[index].name,
                                          style: h3,
                                        ),
                                      ),
                                      pw.SizedBox(
                                        height: 10,
                                      ),
                                      pw.Container(
                                        child: pw.Text(
                                          "Position : ${experienceList[index].position}",
                                          style: pw.TextStyle(
                                              fontSize: 16,
                                              fontWeight: pw.FontWeight.normal),
                                        ),
                                      ),
                                      pw.SizedBox(
                                        height: 5,
                                      ),
                                      pw.Container(
                                        child: pw.Text(
                                          "Working period : ${experienceList[index].period}",
                                          style: pw.TextStyle(
                                              fontSize: 16,
                                              fontWeight: pw.FontWeight.normal),
                                        ),
                                      ),
                                      pw.SizedBox(
                                        height: 5,
                                      ),
                                      pw.Container(
                                        child: pw.Text(
                                          "Job details : ${experienceList[index].project}",
                                          style: pw.TextStyle(
                                              fontSize: 16,
                                              fontWeight: pw.FontWeight.normal),
                                        ),
                                      ),
                                    ],
                                  ),
                                )
                              ],
                            ),
                          );
                        }),
                  ),

                  //Acheivements
                  pw.Container(
                    color: PdfColor.fromHex("#808080"),
                    width: Get.width,
                    margin: pw.EdgeInsets.only(bottom: 10),
                    padding: pw.EdgeInsets.only(left: 10, top: 5, bottom: 5),
                    child: pw.Text(
                      "Achievements",
                      style: h2,
                    ),
                  ),
                  pw.Container(
                    margin: pw.EdgeInsets.only(left: 10, bottom: 15),
                    child: pw.ListView.builder(
                        itemCount: achievementList.length,
                        itemBuilder: (context, index) {
                          return pw.Container(
                            padding: pw.EdgeInsets.all(3),
                            child: pw.Row(
                              children: [
                                pw.Container(
                                  height: 8,
                                  width: 8,
                                  decoration: pw.BoxDecoration(
                                    borderRadius: pw.BorderRadius.all(
                                      pw.Radius.circular(4.0),
                                    ),
                                    color: PdfColor.fromHex("#808080"),
                                  ),
                                ),
                                pw.SizedBox(
                                  width: 10,
                                ),
                                pw.Expanded(
                                  child: pw.Container(
                                    child: pw.Text(
                                      achievementList[index],
                                      style: pw.TextStyle(fontSize: 16),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          );
                        }),
                  ),

                  //Education
                  pw.Container(
                    color: PdfColor.fromHex("#808080"),
                    width: Get.width,
                    margin: pw.EdgeInsets.only(bottom: 10),
                    padding: pw.EdgeInsets.only(left: 10, top: 5, bottom: 5),
                    child: pw.Text(
                      "Education",
                      style: h2,
                    ),
                  ),
                  pw.Container(
                    margin: pw.EdgeInsets.only(left: 0, bottom: 15),
                    child: pw.ListView.builder(
                        itemCount: educationList.length,
                        itemBuilder: (context, index) {
                          return pw.Container(
                            padding: pw.EdgeInsets.all(8),
                            child: pw.Row(
                              crossAxisAlignment: pw.CrossAxisAlignment.start,
                              children: [
                                pw.Container(
                                  margin: pw.EdgeInsets.only(top: 4),
                                  width: 10,
                                  height: 10,
                                  color: PdfColor.fromHex("#808080"),
                                ),
                                pw.SizedBox(
                                  width: 10,
                                ),
                                pw.Expanded(
                                  child: pw.Column(
                                    crossAxisAlignment:
                                        pw.CrossAxisAlignment.start,
                                    children: [
                                      pw.Container(
                                        child: pw.Text(
                                          educationList[index].level,
                                          style: pw.TextStyle(
                                              fontSize: 16,
                                              fontWeight: pw.FontWeight.bold),
                                        ),
                                      ),
                                      pw.SizedBox(
                                        height: 10,
                                      ),
                                      pw.Container(
                                        child: pw.Text(
                                          "Institute : ${educationList[index].instituteName}",
                                          style: pw.TextStyle(
                                              fontSize: 16,
                                              fontWeight: pw.FontWeight.normal),
                                        ),
                                      ),
                                      pw.SizedBox(
                                        height: 5,
                                      ),
                                      pw.Container(
                                        child: pw.Text(
                                          "Year : ${educationList[index].year}",
                                          style: pw.TextStyle(
                                              fontSize: 16,
                                              fontWeight: pw.FontWeight.normal),
                                        ),
                                      ),
                                      pw.SizedBox(
                                        height: 5,
                                      ),
                                      pw.Container(
                                        child: pw.Text(
                                          "GPA/CGPA : ${educationList[index].gpaCgpa}",
                                          style: pw.TextStyle(
                                              fontSize: 16,
                                              fontWeight: pw.FontWeight.normal),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          );
                        }),
                  ),

                  //Language
                  pw.Container(
                    color: PdfColor.fromHex("#808080"),
                    width: Get.width,
                    margin: pw.EdgeInsets.only(bottom: 10),
                    padding: pw.EdgeInsets.only(left: 10, top: 5, bottom: 5),
                    child: pw.Text(
                      "Language Proficiency",
                      style: h2,
                    ),
                  ),
                  pw.Container(
                    margin: pw.EdgeInsets.only(bottom: 10, left: 10),
                    child: pw.ListView.builder(
                        itemCount: languageList.length,
                        itemBuilder: (context, index) {
                          return pw.Container(
                            padding: pw.EdgeInsets.only(top: 8, bottom: 8),
                            child: pw.Row(
                              crossAxisAlignment: pw.CrossAxisAlignment.start,
                              children: [
                                pw.Container(
                                  margin: pw.EdgeInsets.only(top: 4),
                                  width: 10,
                                  height: 10,
                                  color: PdfColor.fromHex("#808080"),
                                ),
                                pw.SizedBox(
                                  width: 10,
                                ),
                                pw.Expanded(
                                  child: pw.Column(
                                    crossAxisAlignment:
                                        pw.CrossAxisAlignment.start,
                                    children: [
                                      pw.Container(
                                        child: pw.Text(
                                          languageList[index].languageName,
                                          style: pw.TextStyle(
                                              fontSize: 17,
                                              fontWeight: pw.FontWeight.bold),
                                        ),
                                      ),
                                      pw.SizedBox(
                                        height: 10,
                                      ),
                                      pw.Container(
                                        child: pw.Text(
                                          "Fluency : ${languageList[index].proficiency}",
                                          style: pw.TextStyle(
                                              fontSize: 16,
                                              fontWeight: pw.FontWeight.normal),
                                        ),
                                      ),
                                      pw.SizedBox(
                                        height: 5,
                                      ),
                                    ],
                                  ),
                                )
                              ],
                            ),
                          );
                        }),
                  ),
                ],
              ),
            );
          }));

      final output = "/storage/emulated/0/Download";
      final file = File("$output/cv.pdf");

      file.writeAsBytesSync(List.from(await doc.save()));

      OpenFile.open("$output/cv.pdf");
    }
  }

  getCV() async {

    isLoading.value = true;

    try {
      var apiResponse = await _httpService.getRequest(
          Apis.GET_CV + "?id=${PrefManager.getString(PrefKeys.USER_ID)}");

      List<dynamic> cvData = apiResponse.data;

      List<ModelCv> value = List<ModelCv>.from(
          cvData.map((i) => ModelCv.fromJson(i)));

      if (apiResponse.statusCode == 200) {
        isLoading.value = false;
        ModelCv item = value[0];
        PrefManager.putString(PrefKeys.CV_NAME, item.name);
        PrefManager.putString(PrefKeys.CV_EMAIL, item.email);
        PrefManager.putString(PrefKeys.CV_ADDRESS, item.address);
        PrefManager.putString(PrefKeys.CV_MOBILE, item.phone);
        PrefManager.putString(PrefKeys.CV_OBJECTIVE, item.careerObjective);
        PrefManager.putString(PrefKeys.CV_SKILLS, item.skills);
        PrefManager.putString(PrefKeys.CV_EXPERIENCE, item.experience);
        PrefManager.putString(PrefKeys.CV_ACHEIVEMENTS, item.achievements);
        PrefManager.putString(PrefKeys.CV_EDUCATION, item.education);
        PrefManager.putString(PrefKeys.CV_LANGUAGE, item.languageProficiency);
        skillsList.value = getSkillList();
        experienceList.value = getExperienceList();
        achievementList.value = getAcievementList();
        educationList.value = getEducationList();
        languageList.value = getLanguageList();
        updateProgress();
      }else {
        isLoading.value = false;
        SnackBarWidget.showMessage("Server Error");
      }
    } on DioError catch (e) {
      isLoading.value = false;
      SnackBarWidget.showMessage(e.message);
    } catch (e) {
      isLoading.value = false;
      // SnackBarWidget.showMessage(e.toString());
        throw e;
    }
  }

  updateProgress() {
    AppConstant.cv_progress.value = 0.0;
    if (PrefManager.getString(PrefKeys.CV_NAME) != "") {
      AppConstant.cv_progress.value += 10.0;
    }
    if (PrefManager.getString(PrefKeys.CV_EMAIL) != "") {
      AppConstant.cv_progress.value += 10.0;
    }
    if (PrefManager.getString(PrefKeys.CV_ADDRESS) != "") {
      AppConstant.cv_progress.value += 10.0;
    }
    if (PrefManager.getString(PrefKeys.CV_MOBILE) != "") {
      AppConstant.cv_progress.value += 10.0;
    }
    if (PrefManager.getString(PrefKeys.CV_OBJECTIVE) != "") {
      AppConstant.cv_progress.value += 10.0;
    }
    if (PrefManager.getString(PrefKeys.CV_SKILLS) != "" &&
        PrefManager.getString(PrefKeys.CV_SKILLS) != "[]") {
      AppConstant.cv_progress.value += 15.0;
    }
    if (PrefManager.getString(PrefKeys.CV_EXPERIENCE) != "" &&
        PrefManager.getString(PrefKeys.CV_EXPERIENCE) != "[]") {
      AppConstant.cv_progress.value += 10.0;
    }
    if (PrefManager.getString(PrefKeys.CV_ACHEIVEMENTS) != "" &&
        PrefManager.getString(PrefKeys.CV_ACHEIVEMENTS) != "[]") {
      AppConstant.cv_progress.value += 5.0;
    }
    if (PrefManager.getString(PrefKeys.CV_EDUCATION) != "" &&
        PrefManager.getString(PrefKeys.CV_EDUCATION) != "[]") {
      AppConstant.cv_progress.value += 15.0;
    }
    if (PrefManager.getString(PrefKeys.CV_LANGUAGE) != "" &&
        PrefManager.getString(PrefKeys.CV_LANGUAGE) != "[]") {
      AppConstant.cv_progress.value += 5.0;
    }
  }
}
