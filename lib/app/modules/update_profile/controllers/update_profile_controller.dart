import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:job_portal/app/data/network/apis.dart';
import 'package:job_portal/app/data/network/http_service.dart';
import 'package:job_portal/app/modules/profile/controllers/profile_controller.dart';
import 'package:job_portal/app/shared_prefrence/pref_keys.dart';
import 'package:job_portal/app/shared_prefrence/pref_manager.dart';
import 'package:job_portal/app/utils/snack_bar.dart';

class UpdateProfileController extends GetxController {

  HttpService _httpService = Get.put(HttpService());
  var profileController = Get.put(ProfileController());

  RxString email = "".obs;
  RxString password = "".obs;
  RxString confirm_password = "".obs;
  RxString phoneNumber = "".obs;
  RxString name = "".obs;
  RxString dateOfBirth = "".obs;
  RxBool isLoading = false.obs;

  TextEditingController nameController = TextEditingController();
  TextEditingController emailController = TextEditingController();
  TextEditingController mobileController = TextEditingController();
  TextEditingController birthdateController = TextEditingController();

  @override
  void onInit() {
    super.onInit();
    nameController.text = PrefManager.getString(PrefKeys.USER_NAME);
    emailController.text = PrefManager.getString(PrefKeys.USER_EMAIL);
    mobileController.text = PrefManager.getString(PrefKeys.USER_MOBILE);
    birthdateController.text = PrefManager.getString(PrefKeys.USER_DOB);
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}

  updateProfile() async {

    isLoading.value = true;

    Map<String, dynamic> data = {
      'id': PrefManager.getString(PrefKeys.USER_ID),
      'name': nameController.text,
      'phone': mobileController.text,
      'birth_date': birthdateController.text,
    };

    try {
      var apiResponse = await _httpService
          .postRequest(Apis.UPDATE_PROFILE, data: data, isFormData: true);

      Map<String, dynamic> mapData = jsonDecode(apiResponse.toString());
      if(mapData['status'] == 200){
        isLoading.value = false;
        PrefManager.putValue(PrefKeys.LOGIN_STATUS, true);
        PrefManager.putValue(PrefKeys.USER_NAME, nameController.text);
        PrefManager.putValue(PrefKeys.USER_MOBILE, mobileController.text);
        PrefManager.putValue(PrefKeys.USER_DOB, birthdateController.text);
        SnackBarWidget.showMessage("Profile Updated Successfully");
        profileController.onReady();
      }else{
        isLoading.value = false;
        SnackBarWidget.showMessage("Server Error");
      }
    } on DioError catch (e) {
      isLoading.value = false;
      SnackBarWidget.showMessage(e.message);
    } catch (e) {
      isLoading.value = false;
      SnackBarWidget.showMessage(e.toString());
      throw e;
    }
  }

}
