import 'package:dio/dio.dart';
import 'package:get/get.dart';
import 'package:job_portal/app/data/model/model_job_post.dart';
import 'package:job_portal/app/data/network/apis.dart';
import 'package:job_portal/app/data/network/http_service.dart';
import 'package:job_portal/app/utils/snack_bar.dart';

class SearchResultController extends GetxController {
  RxBool isLoading = false.obs;
  HttpService _httpService = Get.put(HttpService());
  var itemsList = <ModelJobPost>[].obs;
  RxString selectedLocationID = "".obs;
  RxString selectedCategoryID = "".obs;
  RxString selectedTitleID = "".obs;

  @override
  void onInit() {
    super.onInit();
    selectedTitleID.value = Get.arguments[0].toString();
    selectedLocationID.value = Get.arguments[1].toString();
    selectedCategoryID.value = Get.arguments[2].toString();
  }

  @override
  void onReady() {
    super.onReady();
    getJobs();
  }

  @override
  void onClose() {}

  getJobs() async {
    isLoading.value = true;

    try {
      var apiResponse = await _httpService.postRequest(Apis.HOT_JOBS +
          "?location=${selectedLocationID}&category=${selectedCategoryID}&title=${selectedTitleID}");
      if (apiResponse.statusCode == 200) {
        isLoading.value = false;
        List<dynamic> jobList = apiResponse.data;
        itemsList.value = List<ModelJobPost>.from(
            jobList.map((i) => ModelJobPost.fromJson(i)));
      } else {
        isLoading.value = false;
        SnackBarWidget.showMessage("Server Error");
      }
    } on DioError catch (e) {
      isLoading.value = false;
      SnackBarWidget.showMessage(e.message);
    } catch (e) {
      isLoading.value = false;
      SnackBarWidget.showMessage(e.toString());
      throw e;
    }
  }
}
