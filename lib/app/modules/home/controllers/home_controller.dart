import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:job_portal/app/modules/cv_maker/views/cv_maker_view.dart';
import 'package:job_portal/app/modules/cv_preview/views/cv_preview_view.dart';
import 'package:job_portal/app/modules/hot_jobs/views/hot_jobs_view.dart';
import 'package:job_portal/app/modules/login/views/login_view.dart';
import 'package:job_portal/app/modules/my_jobs/views/my_jobs_view.dart';
import 'package:job_portal/app/modules/profile/views/profile_view.dart';
import 'package:job_portal/app/modules/profile_highlights/views/profile_highlights_view.dart';
import 'package:job_portal/app/modules/search/views/search_view.dart';
import 'package:job_portal/app/modules/splash/views/splash_view.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';

import '../../cv_preview/controllers/cv_preview_controller.dart';

class HomeController extends GetxController {

  late PersistentTabController tab_controller;
  var cvController = Get.put(CvPreviewController());

  @override
  void onInit() {
    super.onInit();
    tab_controller = PersistentTabController(initialIndex: 2);
    cvController.getCV();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}

  List<Widget> buildScreens() {
    return [
      HotJobsView(),
      ProfileHighlightsView(),
      SearchView(),
      MyJobsView(),
      ProfileView()
    ];
  }

  List<PersistentBottomNavBarItem> navBarsItems() {
    return [
      PersistentBottomNavBarItem(
        icon: Icon(CupertinoIcons.home),
        title: ("Hot jobs!"),
        activeColorPrimary: Colors.lightBlueAccent,
        inactiveColorPrimary: CupertinoColors.systemGrey,
      ),
      PersistentBottomNavBarItem(
        icon: Icon(CupertinoIcons.doc_append),
        title: ("Highlights"),
        activeColorPrimary: Colors.lightBlueAccent,
        inactiveColorPrimary: CupertinoColors.systemGrey,
      ),
      PersistentBottomNavBarItem(
        icon: Icon(CupertinoIcons.search, color: Colors.white,),
        title: ("Search"),
        activeColorPrimary: Colors.lightBlueAccent,
        inactiveColorPrimary: CupertinoColors.systemGrey,
      ),
      PersistentBottomNavBarItem(
        icon: Icon(CupertinoIcons.bookmark),
        title: ("My Jobs"),
        activeColorPrimary: Colors.lightBlueAccent,
        inactiveColorPrimary: CupertinoColors.systemGrey,
      ),
      PersistentBottomNavBarItem(
        icon: Icon(CupertinoIcons.profile_circled),
        title: ("Profile"),
        activeColorPrimary: Colors.lightBlueAccent,
        inactiveColorPrimary: CupertinoColors.systemGrey,
      ),
    ];
  }

}
