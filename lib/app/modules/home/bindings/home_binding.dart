import 'package:get/get.dart';
import 'package:job_portal/app/modules/cv_maker/controllers/cv_maker_controller.dart';
import 'package:job_portal/app/modules/cv_preview/controllers/cv_preview_controller.dart';
import 'package:job_portal/app/modules/hot_jobs/controllers/hot_jobs_controller.dart';
import 'package:job_portal/app/modules/my_jobs/controllers/my_jobs_controller.dart';
import 'package:job_portal/app/modules/profile/controllers/profile_controller.dart';
import 'package:job_portal/app/modules/search/controllers/search_controller.dart';

import '../../profile_highlights/controllers/profile_highlights_controller.dart';
import '../controllers/home_controller.dart';

class HomeBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<HomeController>(
      () => HomeController(),
    );

    Get.lazyPut<HotJobsController>(
          () => HotJobsController(),
    );

    Get.lazyPut<CvMakerController>(
          () => CvMakerController(),
    );

    Get.lazyPut<CvPreviewController>(
          () => CvPreviewController(),
    );

    Get.lazyPut<SearchController>(
          () => SearchController(),
    );

    Get.lazyPut<ProfileController>(
          () => ProfileController(),
    );

    Get.lazyPut<MyJobsController>(
          () => MyJobsController(),
    );

    Get.lazyPut<ProfileHighlightsController>(
          () => ProfileHighlightsController(),
    );



  }
}
