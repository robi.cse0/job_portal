import 'package:get/get.dart';

import '../controllers/hot_jobs_controller.dart';

class HotJobsBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<HotJobsController>(
      () => HotJobsController(),
    );
  }
}
