import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:job_portal/app/routes/app_pages.dart';
import 'package:job_portal/app/utils/loading_bar.dart';

import '../controllers/my_jobs_controller.dart';

class MyJobsView extends GetView<MyJobsController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('My Applied Jobs'),
        centerTitle: true,
      ),
      body: Obx(() {
        return controller.isLoading.isFalse
            ? Container(
          padding: EdgeInsets.all(16),
          child: ListView.builder(
              itemCount: controller.itemsList.length,
              itemBuilder: (context, index) {
                return GestureDetector(
                  onTap: () {

                  },
                  child: Card(
                    child: Container(
                      padding: EdgeInsets.all(10),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                            child: Text(
                              controller.itemsList[index].job.title.title,
                              style: TextStyle(
                                  fontSize: 17,
                                  color: Colors.lightBlueAccent,
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                          SizedBox(
                            height: 8,
                          ),
                          Container(
                            child: Text(
                              controller.itemsList[index].job.company.name,
                              style: TextStyle(
                                  fontSize: 16,
                                  color: Colors.grey,
                                  fontWeight: FontWeight.normal),
                            ),
                          ),
                          //Deadline
                          SizedBox(
                            height: 20,
                          ),
                          Container(
                            child: Row(
                              children: [
                                Icon(
                                  Icons.calendar_today,
                                  color: Colors.grey,
                                  size: 20,
                                ),
                                SizedBox(
                                  width: 10,
                                ),
                                Text(
                                  "Applied on",
                                  style: TextStyle(fontSize: 16),
                                )
                              ],
                            ),
                          ),
                          SizedBox(
                            height: 5,
                          ),
                          Container(
                            child: Row(
                              children: [
                                SizedBox(
                                  width: 30,
                                ),
                                Text(
                                    DateFormat('dd-MM-yyyy, hh:mm a').format(controller.itemsList[index].createdAt).toString(),
                                  style: TextStyle(fontSize: 14),
                                )
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                );
              }),
        )
            : Container(
          height: Get.height,
          width: Get.width,
          child: LoadingBar.spinkit,
        );
      }),
    );
  }
}
