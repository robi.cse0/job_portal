import 'package:dio/dio.dart';
import 'package:get/get.dart';
import 'package:job_portal/app/data/model/model_my_application.dart';
import 'package:job_portal/app/data/network/apis.dart';
import 'package:job_portal/app/data/network/http_service.dart';
import 'package:job_portal/app/shared_prefrence/pref_keys.dart';
import 'package:job_portal/app/shared_prefrence/pref_manager.dart';
import 'package:job_portal/app/utils/snack_bar.dart';

class MyJobsController extends GetxController {
  RxBool isLoading = false.obs;
  HttpService _httpService = Get.put(HttpService());
  var itemsList = <Application>[].obs;

  @override
  void onInit() {
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();

    getMyJobs();
  }

  @override
  void onClose() {}

  getMyJobs() async {
    isLoading.value = true;

    try {
      var apiResponse = await _httpService.postRequest(Apis.MY_APPLICATIONS +
          "?id=${PrefManager.getString(PrefKeys.USER_ID)}");
      if (apiResponse.statusCode == 200) {
        isLoading.value = false;
        itemsList.value =
            ModelMyApplication.fromJson(apiResponse.data).applications;
      } else {
        isLoading.value = false;
        SnackBarWidget.showMessage("Server Error");
      }
    } on DioError catch (e) {
      isLoading.value = false;
      SnackBarWidget.showMessage(e.message);
    } catch (e) {
      isLoading.value = false;
      SnackBarWidget.showMessage(e.toString());
      throw e;
    }
  }
}
