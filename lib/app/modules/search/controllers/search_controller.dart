import 'package:dio/dio.dart';
import 'package:get/get.dart';
import 'package:job_portal/app/data/model/model_search_item.dart';
import 'package:job_portal/app/data/network/apis.dart';
import 'package:job_portal/app/data/network/http_service.dart';
import 'package:job_portal/app/routes/app_pages.dart';
import 'package:job_portal/app/utils/snack_bar.dart';

class SearchController extends GetxController {
  RxBool isLoading = false.obs;
  HttpService _httpService = Get.put(HttpService());
  ModelSearchItems modelSearchItems =
      ModelSearchItems(location: [], categories: [], titles: []);

  RxString selectedLocationID = "".obs;
  RxString selectedCategoryID = "".obs;
  RxString selectedTitleID = "".obs;

  @override
  void onInit() {
    super.onInit();
    getSearchItems();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}

  getSearchItems() async {
    isLoading.value = true;
    try {
      var apiResponse = await _httpService.getRequest(Apis.HOT_JOBS);
      if (apiResponse.statusCode == 200) {
        isLoading.value = false;
        Map<String, dynamic> jobList = apiResponse.data;
        modelSearchItems = ModelSearchItems.fromJson(jobList);
        isLoading.value = false;
      } else {
        isLoading.value = false;
        SnackBarWidget.showMessage("Server Error");
      }
    } on DioError catch (e) {
      isLoading.value = false;
      SnackBarWidget.showMessage(e.message);
    } catch (e) {
      isLoading.value = false;
      SnackBarWidget.showMessage(e.toString());
      throw e;
    }
  }

  gotToSearchListPage() {
    Get.toNamed(Routes.SEARCH_RESULT, arguments: [selectedTitleID, selectedLocationID, selectedCategoryID]);
  }
}
