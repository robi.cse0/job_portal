import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:job_portal/app/routes/app_pages.dart';
import 'package:job_portal/app/utils/loading_bar.dart';

import '../controllers/search_controller.dart';
import 'dropdown_search.dart';

class SearchView extends GetView<SearchController> {
  final _formKey = GlobalKey<FormState>();
  var txtITitle = TextEditingController();
  var txtILocation = TextEditingController();
  var txtCategory = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Obx((){
      return Scaffold(
          backgroundColor: Colors.white,
          body: SafeArea(
            child: controller.isLoading.isFalse
                ? SingleChildScrollView(
              child: Form(
                key: _formKey,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Image(
                      image: AssetImage('assets/images/logo.jpeg'),
                      height: 200,
                      width: 250,
                      fit: BoxFit.fill,
                    ),
                    GestureDetector(
                      onTap: () {
                        showDialog(
                            context: context,
                            builder: (context) {
                              return DropdownSearchView(
                                  controller.modelSearchItems,
                                  "Job Title Search", controller);
                            }).then((value) {
                          txtITitle.text = value;
                        });
                      },
                      child: Container(
                        margin: EdgeInsets.only(
                            left: 20, right: 20, top: 10, bottom: 10),
                        child: AbsorbPointer(
                          child: TextFormField(
                            controller: txtITitle,
                            decoration: InputDecoration(
                              suffixIcon: Padding(
                                padding: const EdgeInsets.all(10.0),
                                child: Image(
                                  image: AssetImage(
                                      "assets/images/arrow_down.png"),
                                  height: 15,
                                  width: 15,
                                  color: Colors.grey,
                                ),
                              ),
                              enabledBorder: OutlineInputBorder(
                                borderSide: const BorderSide(
                                    color: Colors.grey, width: 1.5),
                                borderRadius: BorderRadius.circular(5.0),
                              ),
                              border: const OutlineInputBorder(),
                              filled: true,
                              fillColor: Colors.white,
                              focusedBorder: OutlineInputBorder(
                                borderSide: BorderSide(
                                    color: Colors.grey, width: 2.0),
                                borderRadius: BorderRadius.circular(5.0),
                              ),
                              label: Text("Job Title Search"),
                              labelStyle: const TextStyle(fontFamily: ""),
                            ),
                            readOnly: true,
                            showCursor: false,
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    GestureDetector(
                      onTap: () {
                        showDialog(
                            context: context,
                            builder: (context) {
                              return DropdownSearchView(
                                  controller.modelSearchItems,
                                  "Location Search", controller);
                            }).then((value) {
                          txtILocation.text = value;
                        });
                      },
                      child: Container(
                        margin: EdgeInsets.only(
                            left: 20, right: 20, top: 10, bottom: 10),
                        child: AbsorbPointer(
                          child: TextFormField(
                            controller: txtILocation,
                            decoration: InputDecoration(
                              suffixIcon: Padding(
                                padding: const EdgeInsets.all(10.0),
                                child: Image(
                                  image: AssetImage(
                                      "assets/images/arrow_down.png"),
                                  height: 15,
                                  width: 15,
                                  color: Colors.grey,
                                ),
                              ),
                              enabledBorder: OutlineInputBorder(
                                borderSide: const BorderSide(
                                    color: Colors.grey, width: 1.5),
                                borderRadius: BorderRadius.circular(5.0),
                              ),
                              border: const OutlineInputBorder(),
                              filled: true,
                              fillColor: Colors.white,
                              focusedBorder: OutlineInputBorder(
                                borderSide: BorderSide(
                                    color: Colors.grey, width: 2.0),
                                borderRadius: BorderRadius.circular(5.0),
                              ),
                              label: Text("Location Search"),
                              labelStyle: const TextStyle(fontFamily: ""),
                            ),
                            readOnly: true,
                            showCursor: false,
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    GestureDetector(
                      onTap: () {
                        showDialog(
                            context: context,
                            builder: (context) {
                              return DropdownSearchView(
                                  controller.modelSearchItems,
                                  "Category Search", controller);
                            }).then((value) {
                          txtCategory.text = value;
                        });
                      },
                      child: Container(
                        margin: EdgeInsets.only(
                            left: 20, right: 20, top: 10, bottom: 10),
                        child: AbsorbPointer(
                          child: TextFormField(
                            controller: txtCategory,
                            decoration: InputDecoration(
                              suffixIcon: Padding(
                                padding: const EdgeInsets.all(10.0),
                                child: Image(
                                  image: AssetImage(
                                      "assets/images/arrow_down.png"),
                                  height: 15,
                                  width: 15,
                                  color: Colors.grey,
                                ),
                              ),
                              enabledBorder: OutlineInputBorder(
                                borderSide: const BorderSide(
                                    color: Colors.grey, width: 1.5),
                                borderRadius: BorderRadius.circular(5.0),
                              ),
                              border: const OutlineInputBorder(),
                              filled: true,
                              fillColor: Colors.white,
                              focusedBorder: OutlineInputBorder(
                                borderSide: BorderSide(
                                    color: Colors.grey, width: 2.0),
                                borderRadius: BorderRadius.circular(5.0),
                              ),
                              label: Text("Category Search"),
                              labelStyle: const TextStyle(fontFamily: ""),
                            ),
                            readOnly: true,
                            showCursor: false,
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    ElevatedButton(
                        onPressed: () {
                          controller.gotToSearchListPage();
                        },
                        child: Container(
                          width: 200,
                          height: 50,
                          child: Center(child: Text("Search")),
                        ))
                  ],
                ),
              ),
            )
                : LoadingBar.spinkit,
          ));
    });
  }
}
