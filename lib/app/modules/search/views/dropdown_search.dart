import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:job_portal/app/data/model/model_search_item.dart';
import 'package:job_portal/app/modules/search/controllers/search_controller.dart';

class DropdownSearchView extends StatefulWidget {

  ModelSearchItems model;
  String label;
  SearchController controller;

  DropdownSearchView(this.model, this.label, this.controller);

  @override
  _DropdownSearchViewState createState() => _DropdownSearchViewState();
}

class _DropdownSearchViewState extends State<DropdownSearchView> {

  TextEditingController editingController = TextEditingController();

  _DropdownSearchViewState();

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(20),
      child: Scaffold(
        appBar: AppBar(
          elevation: 1.0,
          backgroundColor: Colors.white,
          automaticallyImplyLeading: false,
          leading: IconButton(
            onPressed: () {
              Get.back();
            },
            icon: Icon(
              Icons.arrow_back_outlined,
              color: Colors.black,
            ),
          ),
          title: Text(
            widget.label,
            style: TextStyle(color: Colors.black),
          ),
          centerTitle: true,
        ),
        backgroundColor: Colors.white,
        body: Container(
          margin: EdgeInsets.only(top: 5, left: 20, right: 20),
          color: Colors.white,
          child: Column(children: <Widget>[
            Container(
              padding: EdgeInsets.all(8.0),
              margin: EdgeInsets.only(top: 10),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(8.0),
                  border: Border.all(color: Colors.grey, width: 2)
              ),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Icon(Icons.search, size: 30, color: Colors.grey,),
                  Expanded(
                    child: TextField(
                      onChanged: (value) {
                        setState(() {});
                      },
                      controller: editingController,
                      decoration: InputDecoration(
                        hintText: "Search ${widget.label}",
                        fillColor: Colors.white,
                        border: InputBorder.none,
                      ),
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(height: 5,),
            Expanded(
              child: ListView.builder(
                  shrinkWrap: true,
                  itemCount:
                  widget.label == "Job Title Search" ? widget.model.titles.length:
                  widget.label == "Location Search" ? widget.model.location.length:
                  widget.label == "Category Search" ? widget.model.categories.length:
                  0,
                  itemBuilder: (context, index) {
                    if(widget.label == "Job Title Search"){
                      if (editingController.text.isEmpty || widget.model.titles[index].title.toLowerCase().contains(
                          editingController.text.toString().toLowerCase())) {
                        return GestureDetector(
                          onTap: (){
                            widget.controller.selectedTitleID.value = widget.model.titles[index].id.toString();
                            Navigator.pop(context, widget.model.titles[index].title);
                          },
                          child: getListItem(widget.model.titles[index].title),
                        );
                      } else {
                        return Container();
                      }
                    }else if(widget.label == "Location Search"){
                      if (editingController.text.isEmpty || widget.model.location[index].name.toLowerCase().contains(
                          editingController.text.toString().toLowerCase())) {
                        return GestureDetector(
                          onTap: (){
                            widget.controller.selectedLocationID.value = widget.model.location[index].id.toString();
                            Navigator.pop(context, widget.model.location[index].name);
                          },
                          child: getListItem(widget.model.location[index].name),
                        );

                      } else {
                        return Container();
                      }
                    }else{
                      if (editingController.text.isEmpty || widget.model.categories[index].category.toLowerCase().contains(
                          editingController.text.toString().toLowerCase())) {
                        return GestureDetector(
                          onTap: (){
                            widget.controller.selectedCategoryID.value = widget.model.categories[index].id.toString();
                            Navigator.pop(context, widget.model.categories[index].category);
                          },
                          child: getListItem(widget.model.categories[index].category),
                        );
                      } else {
                        return Container();
                      }
                    }

                  }),
            ),
          ]),
        ),
      ),
    );
  }

  getListItem(String text){
    return Container(
      color: Colors.white,
      padding: EdgeInsets.only(top: 13, bottom: 12),
      child: Row(
        children: [
          Container(width: 13,),
          Text(text, style: TextStyle(
              fontSize: 16,
              color: Colors.black
          ),)
        ],
      ),
    );
  }
}
