import 'package:get/get.dart';

import '../controllers/profile_highlights_controller.dart';

class ProfileHighlightsBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<ProfileHighlightsController>(
      () => ProfileHighlightsController(),
    );
  }
}
