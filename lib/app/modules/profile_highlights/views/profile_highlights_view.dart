import 'package:flutter/material.dart';

import 'package:get/get.dart';

import '../../../utils/gridview_delegate.dart';
import '../../../utils/loading_bar.dart';
import '../controllers/profile_highlights_controller.dart';

class ProfileHighlightsView extends GetView<ProfileHighlightsController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Highlights'),
        centerTitle: true,
      ),
      body: Obx(() {
        return controller.isLoading.isFalse
            ? Container(
          padding: EdgeInsets.all(16),
          child: GridView.builder(
              itemCount: controller.itemsList.length,
              itemBuilder: (context, index) {
                return GestureDetector(
                  onTap: () {

                  },
                  child: Card(
                    child: Container(
                      padding: EdgeInsets.all(10),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          ClipRRect(
                            borderRadius: BorderRadius.circular(75.0),
                            child: SizedBox(
                              height: 70.0,
                              width: 70.0,
                              child: controller.itemsList[index].image == ""?
                              Image.asset("assets/images/avatar.png"):
                              Image.network(
                                "https://mazharsany.xyz/jobs-today/storage/app/"+controller.itemsList[index].image,
                                fit: BoxFit.cover,
                              ),
                            ),
                          ),
                          SizedBox(
                            height: 8,
                          ),
                          Container(
                            child: Text(
                              controller.itemsList[index].name,
                              style: TextStyle(
                                  fontSize: 16,
                                  color: Colors.lightBlueAccent,
                                  fontWeight: FontWeight.bold),
                              textAlign: TextAlign.center,
                            ),

                          ),
                          SizedBox(
                            height: 8,
                          ),
                          Container(
                            child: Text(
                              controller.itemsList[index].email,
                              style: TextStyle(
                                  fontSize: 12,
                                  color: Colors.grey,
                                  fontWeight: FontWeight.normal),
                              textAlign: TextAlign.center,
                            ),

                          ),
                        ],
                      ),
                    ),
                  ),
                );
              },gridDelegate:
              SliverGridDelegateWithFixedCrossAxisCountAndFixedHeight(
              crossAxisCount: 2,
              crossAxisSpacing: 10,
              mainAxisSpacing: 10,
              height: 220),
        ))
            : Container(
          height: Get.height,
          width: Get.width,
          child: LoadingBar.spinkit,
        );
      }),
    );
  }
}
