import 'package:dio/dio.dart';
import 'package:get/get.dart';
import 'package:job_portal/app/data/model/model_highlight_user.dart';

import '../../../data/network/apis.dart';
import '../../../data/network/http_service.dart';
import '../../../utils/snack_bar.dart';

class ProfileHighlightsController extends GetxController {

  RxBool isLoading = false.obs;
  HttpService _httpService = Get.put(HttpService());
  var itemsList = <Datum>[].obs;

  @override
  void onInit() {
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
    getHighlightedUser();
  }

  @override
  void onClose() {}

  getHighlightedUser() async {
    isLoading.value = true;

    try {
      var apiResponse = await _httpService.getRequest(Apis.HIGHLIGHTED_USER);
      if (apiResponse.statusCode == 200) {
        isLoading.value = false;
        itemsList.value =
            ModelHighlightUser.fromJson(apiResponse.data).data;
      } else {
        isLoading.value = false;
        SnackBarWidget.showMessage("Server Error");
      }
    } on DioError catch (e) {
      isLoading.value = false;
      SnackBarWidget.showMessage(e.message);
    } catch (e) {
      isLoading.value = false;
      SnackBarWidget.showMessage(e.toString());
      throw e;
    }
  }
}
