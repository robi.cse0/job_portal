import 'package:get/get.dart';

import '../controllers/registration_user_controller.dart';

class RegistrationUserBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<RegistrationUserController>(
      () => RegistrationUserController(),
    );
  }
}
