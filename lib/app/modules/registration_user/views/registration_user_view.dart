import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:job_portal/app/routes/app_pages.dart';
import 'package:job_portal/app/utils/loading_bar.dart';

import '../controllers/registration_user_controller.dart';

class RegistrationUserView extends GetView<RegistrationUserController> {
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {

    final format = DateFormat("dd-MM-yyyy");

    return Scaffold(
        backgroundColor: Colors.white,
        body: Obx(
            (){
              return SafeArea(
                child: Container(
                  padding: EdgeInsets.all(16),
                  child: SingleChildScrollView(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Center(
                          child: Image(
                            image: AssetImage("assets/images/logo.jpeg"),
                            height: 150,
                          ),
                        ),
                        Text(
                          "SIGN UP",
                          style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        Form(
                            autovalidateMode: AutovalidateMode.onUserInteraction,
                            key: _formKey,
                            child: Column(
                              children: [
                                //name
                                Container(
                                  margin: EdgeInsets.only(top: 10, bottom: 10),
                                  child: TextFormField(
                                    decoration: InputDecoration(
                                      enabledBorder: OutlineInputBorder(
                                        borderSide: const BorderSide(
                                            color: Colors.grey, width: 1.5),
                                        borderRadius: BorderRadius.circular(5.0),
                                      ),
                                      border: const OutlineInputBorder(),
                                      filled: true,
                                      fillColor: Colors.white,
                                      focusedBorder: OutlineInputBorder(
                                        borderSide: BorderSide(
                                            color: Colors.grey, width: 2.0),
                                        borderRadius: BorderRadius.circular(5.0),
                                      ),
                                      label: Text("Name"),
                                      labelStyle: const TextStyle(fontFamily: ""),
                                    ),
                                    keyboardType: TextInputType.emailAddress,
                                    onChanged: (value) {
                                      controller.name.value = value;
                                    },
                                    validator: (value) {
                                      if (value!.isEmpty) {
                                        return 'Please enter your name';
                                      }
                                    },
                                  ),
                                ),
                                //email
                                Container(
                                  child: TextFormField(
                                    decoration: InputDecoration(
                                      enabledBorder: OutlineInputBorder(
                                        borderSide: const BorderSide(
                                            color: Colors.grey, width: 1.5),
                                        borderRadius: BorderRadius.circular(5.0),
                                      ),
                                      border: const OutlineInputBorder(),
                                      filled: true,
                                      fillColor: Colors.white,
                                      focusedBorder: OutlineInputBorder(
                                        borderSide: BorderSide(
                                            color: Colors.grey, width: 2.0),
                                        borderRadius: BorderRadius.circular(5.0),
                                      ),
                                      label: Text("Email"),
                                      labelStyle: const TextStyle(fontFamily: ""),
                                    ),
                                    keyboardType: TextInputType.emailAddress,
                                    onChanged: (value) {
                                      controller.email.value = value;
                                    },
                                    validator: (value) {
                                      if (value!.isEmpty || !value.isEmail) {
                                        return 'Please please provide a valid email';
                                      }
                                    },
                                  ),
                                ),
                                //phone number
                                Container(
                                  margin: EdgeInsets.only(top: 10, bottom: 10),
                                  child: TextFormField(
                                    decoration: InputDecoration(
                                      enabledBorder: OutlineInputBorder(
                                        borderSide: const BorderSide(
                                            color: Colors.grey, width: 1.5),
                                        borderRadius: BorderRadius.circular(5.0),
                                      ),
                                      border: const OutlineInputBorder(),
                                      filled: true,
                                      fillColor: Colors.white,
                                      focusedBorder: OutlineInputBorder(
                                        borderSide: BorderSide(
                                            color: Colors.grey, width: 2.0),
                                        borderRadius: BorderRadius.circular(5.0),
                                      ),
                                      label: Text("Phone number"),
                                      labelStyle: const TextStyle(fontFamily: ""),
                                    ),
                                    keyboardType: TextInputType.number,
                                    onChanged: (value) {
                                      controller.phoneNumber.value = value;
                                    },
                                    validator: (value) {
                                      if (value!.length < 11) {
                                        return 'Please please provide a valid phone number';
                                      }
                                    },
                                  ),
                                ),
                                //DOB
                                Container(
                                  child: DateTimeField(
                                    decoration: InputDecoration(
                                      enabledBorder: OutlineInputBorder(
                                        borderSide: const BorderSide(color: Colors.grey, width: 1.5),
                                        borderRadius: BorderRadius.circular(5.0),
                                      ),
                                      border: const OutlineInputBorder(),
                                      filled: true,
                                      fillColor: Colors.white,
                                      focusedBorder: OutlineInputBorder(
                                        borderSide: BorderSide(color: Colors.grey, width: 2.0),
                                        borderRadius: BorderRadius.circular(5.0),
                                      ),
                                      suffixIcon: Container(
                                        height: 10,
                                        width: 10,
                                        child: Padding(
                                          padding: const EdgeInsets.all(7.0),
                                          child: Image.asset(
                                            'assets/images/calendar.png',
                                          ),
                                        ),
                                      ),
                                      label: Text("Date of birth (Optional)"),
                                      labelStyle: const TextStyle(fontFamily: ""),
                                    ),
                                    format: format,
                                    onShowPicker: (context, currentValue) {
                                      return showDatePicker(
                                          context: context,
                                          firstDate: DateTime(1900),
                                          initialDate: currentValue ?? DateTime.now(),
                                          lastDate: DateTime(2100));
                                    },
                                    onChanged: (val) {
                                      controller.dateOfBirth.value = val.toString();
                                    },
                                    validator: (value) {

                                    },
                                  ),
                                ),
                                //password
                                Container(
                                  margin: EdgeInsets.only(top: 10, bottom: 10),
                                  child: TextFormField(
                                    decoration: InputDecoration(
                                      enabledBorder: OutlineInputBorder(
                                        borderSide: const BorderSide(
                                            color: Colors.grey, width: 1.5),
                                        borderRadius: BorderRadius.circular(5.0),
                                      ),
                                      border: const OutlineInputBorder(),
                                      filled: true,
                                      fillColor: Colors.white,
                                      focusedBorder: OutlineInputBorder(
                                        borderSide: BorderSide(
                                            color: Colors.grey, width: 2.0),
                                        borderRadius: BorderRadius.circular(5.0),
                                      ),
                                      label: Text("Password"),
                                      labelStyle: const TextStyle(fontFamily: ""),
                                    ),
                                    keyboardType: TextInputType.text,
                                    onChanged: (value) {
                                      controller.password.value = value;
                                    },
                                    obscureText: true,
                                    validator: (value) {
                                      if (value!.length < 6) {
                                        return 'Password must be at least 6 digits';
                                      }
                                    },
                                  ),
                                ),
                                //Confirm passowrd
                                Container(
                                  margin: EdgeInsets.only(top: 10, bottom: 10),
                                  child: TextFormField(
                                    decoration: InputDecoration(
                                      enabledBorder: OutlineInputBorder(
                                        borderSide: const BorderSide(
                                            color: Colors.grey, width: 1.5),
                                        borderRadius: BorderRadius.circular(5.0),
                                      ),
                                      border: const OutlineInputBorder(),
                                      filled: true,
                                      fillColor: Colors.white,
                                      focusedBorder: OutlineInputBorder(
                                        borderSide: BorderSide(
                                            color: Colors.grey, width: 2.0),
                                        borderRadius: BorderRadius.circular(5.0),
                                      ),
                                      label: Text("Confirm Password"),
                                      labelStyle: const TextStyle(fontFamily: ""),
                                    ),
                                    keyboardType: TextInputType.text,
                                    onChanged: (value) {
                                      controller.confirm_password.value = value;
                                    },
                                    obscureText: true,
                                    validator: (value) {
                                      if (value!.length < 6 || controller.password != controller.confirm_password) {
                                        return 'Password no matched';
                                      }
                                    },
                                  ),
                                ),
                                SizedBox(
                                  height: 40,
                                ),
                                controller.isLoading.isFalse?Container(
                                  width: double.infinity,
                                  height: 50,
                                  child: ElevatedButton(
                                      onPressed: () {
                                        if (_formKey.currentState!.validate() ==
                                            true) {
                                          controller.register_user();
                                        }
                                      },
                                      child: Text("SIGN UP")),
                                ):LoadingBar.spinkit,
                                SizedBox(
                                  height: 30,
                                ),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Text("Already have a account?"),
                                    SizedBox(
                                      width: 5,
                                    ),
                                    GestureDetector(
                                      onTap: () {
                                        Get.toNamed(Routes.LOGIN);
                                      },
                                      child: Text(
                                        "SIGN IN",
                                        style: TextStyle(color: Colors.blue),
                                      ),
                                    )
                                  ],
                                )
                              ],
                            ))
                      ],
                    ),
                  ),
                ),
              );
            }
        )
    );
  }
}
