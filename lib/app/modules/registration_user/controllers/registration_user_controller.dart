import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:get/get.dart';
import 'package:job_portal/app/data/model/model_auth.dart';
import 'package:job_portal/app/data/network/apis.dart';
import 'package:job_portal/app/data/network/http_service.dart';
import 'package:job_portal/app/routes/app_pages.dart';
import 'package:job_portal/app/utils/snack_bar.dart';

class RegistrationUserController extends GetxController {

  HttpService _httpService = Get.put(HttpService());

  RxString email = "".obs;
  RxString password = "".obs;
  RxString confirm_password = "".obs;
  RxString phoneNumber = "".obs;
  RxString name = "".obs;
  RxString dateOfBirth = "".obs;
  RxBool isLoading = false.obs;

  @override
  void onInit() {
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}

  register_user() async {

    isLoading.value = true;

    Map<String, dynamic> data = {
      'name': name.value,
      'email': email.value,
      'phone': phoneNumber.value,
      'birth_date': dateOfBirth.value,
      'password': confirm_password.value
    };

    try {
      var apiResponse = await _httpService
          .postRequest(Apis.REGISTRATION, data: data, isFormData: true);

      Map<String, dynamic> mapData = jsonDecode(apiResponse.toString());
      if(mapData['status'] == 200){
        isLoading.value = false;
        SnackBarWidget.showMessage("Registration Successful");
        // var model =  ModelRegistration.fromJson(mapData);
        Get.offAllNamed(Routes.LOGIN);
      }else if (mapData['status'] == 500){
        isLoading.value = false;
        SnackBarWidget.showMessage("User already exists");
      }else{
        isLoading.value = false;
        SnackBarWidget.showMessage("Server Error");
      }
    } on DioError catch (e) {
      isLoading.value = false;
      SnackBarWidget.showMessage(e.message);
    } catch (e) {
      isLoading.value = false;
      SnackBarWidget.showMessage(e.toString());
      throw e;
    }
  }

}
