import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:job_portal/app/data/model/model_auth.dart';
import 'package:job_portal/app/data/network/apis.dart';
import 'package:job_portal/app/data/network/http_service.dart';
import 'package:job_portal/app/routes/app_pages.dart';
import 'package:job_portal/app/shared_prefrence/pref_keys.dart';
import 'package:job_portal/app/shared_prefrence/pref_manager.dart';
import 'package:job_portal/app/utils/snack_bar.dart';

class LoginController extends GetxController {

  HttpService _httpService = Get.put(HttpService());

  RxString email = "".obs;
  RxString password = "".obs;
  RxBool isLoading = false.obs;

  @override
  void onInit() {
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}

  checkLogin() async {

    isLoading.value = true;

    Map<String, dynamic> data = {
      'email': email.value,
      'password': password.value
    };

    try {
      var apiResponse = await _httpService
          .postRequest(Apis.LOGIN, data: data, isFormData: true);

      Map<String, dynamic> mapData = jsonDecode(apiResponse.toString());
      if(mapData['status'] == 200){
        isLoading.value = false;
        ModelAuth model =  ModelAuth.fromJson(mapData);
        PrefManager.putValue(PrefKeys.LOGIN_STATUS, true);
        PrefManager.putValue(PrefKeys.USER_NAME, model.data.name);
        PrefManager.putValue(PrefKeys.USER_EMAIL, model.data.email);
        PrefManager.putValue(PrefKeys.USER_ID, model.data.id);
        PrefManager.putValue(PrefKeys.USER_MOBILE, model.data.phone);
        PrefManager.putValue(PrefKeys.USER_DOB, model.data.birthDate);
        PrefManager.putValue(PrefKeys.USER_IMAGE, model.data.image);
        Get.offAllNamed(Routes.HOME);
      }else if (mapData['status'] == 404){
        isLoading.value = false;
        SnackBarWidget.showMessage("Password not matched or User is not registered yet");
      }else{
        isLoading.value = false;
        SnackBarWidget.showMessage("Server Error");
      }
    } on DioError catch (e) {
      isLoading.value = false;
      SnackBarWidget.showMessage(e.message);
    } catch (e) {
      isLoading.value = false;
      SnackBarWidget.showMessage(e.toString());
      throw e;
    }
  }

}
