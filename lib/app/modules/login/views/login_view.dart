import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:job_portal/app/routes/app_pages.dart';
import 'package:job_portal/app/shared_prefrence/pref_keys.dart';
import 'package:job_portal/app/shared_prefrence/pref_manager.dart';
import 'package:job_portal/app/utils/loading_bar.dart';

import '../controllers/login_controller.dart';

class LoginView extends GetView<LoginController> {
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        body: Obx((){
          return SafeArea(
            child: Container(
              padding: EdgeInsets.all(16),
              child: SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Image(
                      image: AssetImage("assets/images/logo.jpeg"),
                    ),
                    Text(
                      "SIGN IN",
                      style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Form(
                        autovalidateMode: AutovalidateMode.onUserInteraction,
                        key: _formKey,
                        child: Column(
                          children: [
                            Container(
                              margin: EdgeInsets.only(top: 10, bottom: 10),
                              child: TextFormField(
                                decoration: InputDecoration(
                                  enabledBorder: OutlineInputBorder(
                                    borderSide: const BorderSide(
                                        color: Colors.grey, width: 1.5),
                                    borderRadius: BorderRadius.circular(5.0),
                                  ),
                                  border: const OutlineInputBorder(),
                                  filled: true,
                                  fillColor: Colors.white,
                                  focusedBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                        color: Colors.grey, width: 2.0),
                                    borderRadius: BorderRadius.circular(5.0),
                                  ),
                                  label: Text("Email"),
                                  labelStyle: const TextStyle(fontFamily: ""),
                                ),
                                keyboardType: TextInputType.emailAddress,
                                onChanged: (value) {
                                  controller.email.value = value;
                                },
                                validator: (value) {
                                  if (value!.isEmpty || !value.isEmail) {
                                    return 'Please enter your email';
                                  }
                                },
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(top: 10, bottom: 10),
                              child: TextFormField(
                                decoration: InputDecoration(
                                  enabledBorder: OutlineInputBorder(
                                    borderSide: const BorderSide(
                                        color: Colors.grey, width: 1.5),
                                    borderRadius: BorderRadius.circular(5.0),
                                  ),
                                  border: const OutlineInputBorder(),
                                  filled: true,
                                  fillColor: Colors.white,
                                  focusedBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                        color: Colors.grey, width: 2.0),
                                    borderRadius: BorderRadius.circular(5.0),
                                  ),
                                  label: Text("Password"),
                                  labelStyle: const TextStyle(fontFamily: ""),
                                ),
                                keyboardType: TextInputType.text,
                                onChanged: (value) {
                                  controller.password.value = value;
                                },
                                obscureText: true,
                                validator: (value) {
                                  if (value!.length < 6) {
                                    return 'Password must be at least 6 digits';
                                  }
                                },
                              ),
                            ),
                            SizedBox(
                              height: 40,
                            ),
                            controller.isLoading.isFalse?Container(
                              width: double.infinity,
                              height: 50,
                              child: ElevatedButton(
                                  onPressed: () {
                                    if(_formKey.currentState!.validate() == true){
                                      controller.checkLogin();
                                    }
                                  },
                                  child: Text("SIGN IN")),
                            ):LoadingBar.spinkit,
                            SizedBox(height: 30,),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Text("Not haven't account?"),
                                SizedBox(width: 5,),
                                GestureDetector(
                                  onTap: (){
                                    Get.toNamed(Routes.REGISTRATION_USER);
                                  },
                                  child: Text("SIGNUP", style: TextStyle(color: Colors.blue),),
                                )
                              ],
                            )
                          ],
                        ))
                  ],
                ),
              ),
            ),
          );
        })
    );
  }
}
