import 'package:get/get.dart';

import '../controllers/update_profile_picture_controller.dart';

class UpdateProfilePictureBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<UpdateProfilePictureController>(
      () => UpdateProfilePictureController(),
    );
  }
}
