import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:job_portal/app/shared_prefrence/pref_keys.dart';
import 'package:job_portal/app/shared_prefrence/pref_manager.dart';
import 'package:job_portal/app/utils/loading_bar.dart';

import '../controllers/update_profile_picture_controller.dart';

class UpdateProfilePictureView extends GetView<UpdateProfilePictureController> {

  String imageFromFilePath = "";

  @override
  Widget build(BuildContext context) {
    return Obx(() {
      return Scaffold(
        appBar: AppBar(
          title: Text('Update Profile Picture'),
          centerTitle: true,
        ),
        body: controller.isLoading.isFalse
            ? Column(
                children: [
                  SizedBox(
                    height: 20,
                  ),
                  Center(
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(75.0),
                      child: SizedBox(
                        height: 150.0,
                        width: 150.0,
                        child: Image.network(
                          controller.imageUrl.value == ""
                              ? "https://png.pngitem.com/pimgs/s/649-6490124_katie-notopoulos-katienotopoulos-i-write-about-tech-round.png"
                              :controller.imageUrl.value,
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  InkWell(
                    onTap: (){
                      controller.imgFromGallery().then((value) {
                        imageFromFilePath = value;
                        print(imageFromFilePath);
                        controller.uploadDocument(imageFromFilePath);
                      });
                    },
                    child: Container(
                      width: 250,
                      height: 50,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(25),
                          color: Colors.lightBlueAccent),
                      child: Center(
                        child: Text(
                          "Change Profile Picture",
                          style: TextStyle(
                              color: Colors.white, fontWeight: FontWeight.bold,fontSize: 18),
                        ),
                      ),
                    ),
                  )
                ],
              )
            : Container(
                height: Get.height,
                width: Get.width,
                child: LoadingBar.spinkit,
              ),
      );
    });
  }
}
