import 'dart:convert';

import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:job_portal/app/data/network/apis.dart';
import 'package:job_portal/app/data/network/http_service.dart';
import 'package:job_portal/app/modules/profile/controllers/profile_controller.dart';
import 'package:job_portal/app/shared_prefrence/pref_keys.dart';
import 'package:job_portal/app/shared_prefrence/pref_manager.dart';

class UpdateProfilePictureController extends GetxController {
  HttpService _httpService = Get.put(HttpService());
  var profileController = Get.put(ProfileController());
  late ImagePicker _picker;
  RxBool isLoading = false.obs;
  RxString imageUrl = "".obs;

  @override
  void onInit() {
    super.onInit();
    _picker = ImagePicker();
  }

  @override
  void onReady() {
    super.onReady();
    imageUrl.value = "https://mazharsany.xyz/jobs-today/storage/app/" +
        PrefManager.getString(PrefKeys.USER_IMAGE);
  }

  @override
  void onClose() {}

  Future<String> imgFromGallery() async {
    if (_picker != null) {
      XFile? image = await _picker.pickImage(source: ImageSource.gallery);
      return image!.path.toString();
    } else {
      return "";
    }
  }

  uploadDocument(String imagePath) async {
    isLoading.value = true;
    try {
      var apiResponse =
          await _httpService.uploadDocument(Apis.UPDATE_IMAGE, imagePath);

      Map<String, dynamic> mapData = jsonDecode(apiResponse.toString());

      if (mapData['status'] == 200) {
        isLoading.value = false;
        PrefManager.putString(
            PrefKeys.USER_IMAGE, mapData["data"]["image"]);
        imageUrl.value = "https://mazharsany.xyz/jobs-today/storage/app/" + mapData["data"]["image"];
        profileController.imageUrl.value = "https://mazharsany.xyz/jobs-today/storage/app/" + mapData["data"]["image"];
      } else {
        isLoading.value = false;
      }
    } catch (e) {
      isLoading.value = false;
      throw e;
    }
  }
}
