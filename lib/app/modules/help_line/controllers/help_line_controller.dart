import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:job_portal/app/data/model/model_message.dart';
import 'package:job_portal/app/shared_prefrence/pref_keys.dart';
import 'package:job_portal/app/shared_prefrence/pref_manager.dart';

class HelpLineController extends GetxController {

  TextEditingController messageController = TextEditingController();

  var messageList = <Messages>[].obs;
  List contactList = [];
  RxBool hl = false.obs;
  
  @override
  void onInit() async {
    super.onInit();
    await Firebase.initializeApp();
    messageList.bindStream(streamDemo());
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}

  isUser(String text) {
    if (text == "user") {
      return true;
    } else {
      return false;
    }
  }

  sendMessage() {
    final docUser = FirebaseFirestore.instance
        .collection("chats")
        .doc(PrefManager.getInt(PrefKeys.USER_ID).toString());
    
    DateTime now = DateTime.now();
    String formattedDate = DateFormat('dd/MM/yyyy, HH:mm:ss').format(now);

    Map<String,String> data = {
      "created_at":formattedDate,
      "message":messageController.text,
      "sent_by":"user"
    };

    contactList.add(data);

    final json = {
      'last_message': formattedDate,
      'messages' : contactList,
      'user': {
        'birth_date': PrefManager.getString(PrefKeys.USER_DOB),
        'email': PrefManager.getString(PrefKeys.USER_EMAIL),
        'gender': "female",
        'image': PrefManager.getString(PrefKeys.USER_IMAGE),
        'name': PrefManager.getString(PrefKeys.USER_NAME),
        'phone': PrefManager.getString(PrefKeys.USER_MOBILE),
      }
    };
    docUser.set(json);
    messageController.text = "";
  }

  Future fetchAllContact() async {
    contactList = [];
    DocumentSnapshot documentSnapshot = await FirebaseFirestore.instance
        .collection('chats')
        .doc(PrefManager.getInt(PrefKeys.USER_ID).toString())
        .get();
    if(documentSnapshot.exists){
      contactList = documentSnapshot['messages'];
    }
  }

  Stream<List<Messages>> streamDemo() {
    return FirebaseFirestore.instance
        .collection('chats')
        .doc(PrefManager.getInt(PrefKeys.USER_ID).toString())
        .snapshots()
        .map((ds) {
      var mapData = ds.data();
      List mapList = mapData!['messages'];
      contactList.addAll(mapList);
      List<Messages> newsModelList = [];
      mapList.forEach((element) {
        newsModelList.add(Messages(element["created_at"], element["message"], element["sent_by"]));
      });
      return newsModelList;
    });
  }

}
