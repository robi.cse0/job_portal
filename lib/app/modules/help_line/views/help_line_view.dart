import 'package:flutter/material.dart';

import 'package:get/get.dart';

import '../controllers/help_line_controller.dart';

class HelpLineView extends GetView<HelpLineController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Help Center'),
        centerTitle: true,
      ),
      body: Obx(() {
        controller.hl.value = false;
        return Container(
          height: Get.height,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              Expanded(
                child: SingleChildScrollView(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      Container(
                        padding: EdgeInsets.all(16.0),
                        child: ListView.builder(
                            shrinkWrap: true,
                            physics: NeverScrollableScrollPhysics(),
                            itemCount: controller.messageList.length,
                            itemBuilder: (context, index) {
                              return Row(
                                children: [
                                  controller.isUser(
                                          controller.messageList[index].sent_by!)
                                      ? Expanded(child: Container())
                                      : Container(),
                                  Container(
                                    padding: EdgeInsets.all(10),
                                    margin: EdgeInsets.only(top: 7),
                                    decoration: BoxDecoration(
                                        color: !controller.isUser(controller
                                                .messageList[index].sent_by!)
                                            ? Colors.white
                                            : Colors.lightBlueAccent,
                                        borderRadius: BorderRadius.circular(8.0)),
                                    child: Text(
                                      controller.messageList[index].message!,
                                      style: TextStyle(
                                          color: !controller.isUser(controller
                                                  .messageList[index].sent_by!)
                                              ? Colors.black
                                              : Colors.white,
                                          fontSize: 16),
                                    ),
                                  ),
                                  !controller.isUser(
                                          controller.messageList[index].sent_by!)
                                      ? Expanded(child: Container())
                                      : Container()
                                ],
                              );
                            }),
                      )
                    ],
                  ),
                ),
              ),
              Container(
                margin: EdgeInsets.only(left: 16, right: 10, bottom: 5),
                child: Row(
                  children: [
                    Expanded(
                      child: Container(
                        height: 60,
                        padding: EdgeInsets.only(left: 30, right: 30),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(30.0),
                            color: Colors.white),
                        child: Center(
                          child: TextFormField(
                            controller: controller.messageController,
                            style: TextStyle(color: Colors.black),
                            decoration: InputDecoration(
                              border: InputBorder.none,
                              hintText: "Please write something",
                            ),
                          ),
                        ),
                      ),
                    ),
                    InkWell(
                      onTap: () {
                        if (controller.messageController.text.isNotEmpty) {
                          controller.fetchAllContact().then((value) {
                            controller.sendMessage();
                          });
                        }
                      },
                      child: Container(
                        height: 50,
                        width: 50,
                        padding: EdgeInsets.all(3.0),
                        child: Image.asset("assets/images/ic_send.png"),
                      ),
                    )
                  ],
                ),
              )
            ],
          ),
        );
      }),
    );
  }
}
