import 'package:get/get.dart';

import '../controllers/help_line_controller.dart';

class HelpLineBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<HelpLineController>(
      () => HelpLineController(),
    );
  }
}
