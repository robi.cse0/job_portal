import 'package:flutter/material.dart';
import 'package:job_portal/app/modules/quiz/controllers/quiz_controller.dart';

class QOptions extends StatefulWidget {
  String question;
  String option1;
  String option2;
  String option3;
  String option4;
  String c_ans;
  int index;
  QuizController controller;

  QOptions(this.question, this.option1, this.option2, this.option3,
      this.option4, this.c_ans, this.index, this.controller);

  @override
  _QOptionsState createState() => _QOptionsState();
}

class _QOptionsState extends State<QOptions>
    with AutomaticKeepAliveClientMixin {
  String selectedValue = "";

  @override
  Widget build(BuildContext context) {
    List<String> opt_list = [
      widget.option1,
      widget.option2,
      widget.option3,
      widget.option4,
    ];

    return Card(
      child: Container(
        padding: EdgeInsets.all(16.0),
        margin: EdgeInsets.only(top: 7),
        child: SingleChildScrollView(
          physics: NeverScrollableScrollPhysics(),
          child: Column(
            children: [
              Container(
                child: Row(
                  children: [
                    Expanded(
                      child: Text(
                        "${widget.index + 1}. ${widget.question}",
                        style: TextStyle(fontWeight: FontWeight.bold),
                        textAlign: TextAlign.start,
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 20,
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: opt_list.map(
                  (item) {
                    return Column(
                      children: [
                        Row(
                          children: [
                            Radio(
                                value: item,
                                groupValue: selectedValue,
                                activeColor: Colors.green,
                                onChanged: (value) => {
                                      setState(() {
                                        selectedValue = item;
                                        if (item == widget.c_ans) {
                                          Map<String, dynamic> data = {
                                            "index": "${widget.index}",
                                            "value": "1"
                                          };
                                          widget.controller.scoreCount(data);
                                          print(
                                              "Count : ${widget.controller.count} \n${value}");
                                        } else {
                                          Map<String, dynamic> data = {
                                            "index": "${widget.index}",
                                            "value": "0"
                                          };
                                          widget.controller.scoreCount(data);
                                          print(
                                              "Count : ${widget.controller.count} \n${value}");
                                        }
                                      })
                                    }),
                            Expanded(
                                child: GestureDetector(
                              onTap: () {
                                setState(() {
                                  selectedValue = item;
                                });
                              },
                              child: Container(
                                  padding: EdgeInsets.only(
                                      left: 10, top: 3, bottom: 3),
                                  child: Text(
                                    item,
                                    style: TextStyle(
                                        fontSize: 17,
                                        fontWeight: FontWeight.normal),
                                  )),
                            ))
                          ],
                        ),
                        SizedBox(
                          height: 3,
                        ),
                      ],
                    );
                  },
                ).toList(),
              ),
            ],
          ),
        ),
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;
}
