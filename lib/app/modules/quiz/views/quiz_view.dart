import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:job_portal/app/routes/app_pages.dart';
import 'package:job_portal/app/utils/loading_bar.dart';

import '../controllers/quiz_controller.dart';
import 'options.dart';

class QuizView extends GetView<QuizController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Quiz'),
        centerTitle: true,
      ),
      body: Obx(() {
        return Column(
          children: [
            Container(
              child: Center(
                child: Text(
                  "${controller.timeLeft.value}",
                  style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 16),
                ),
              ),
              width: Get.width,
              height: 50,
              color: Colors.lightBlue,
            ),
            Expanded(
              child: Container(
                padding: EdgeInsets.all(16.0),
                child: controller.isLoading.isFalse
                    ? SingleChildScrollView(
                        child: Column(
                          children: [
                            ListView.builder(
                                physics: NeverScrollableScrollPhysics(),
                                shrinkWrap: true,
                                itemCount: controller.quizList.length,
                                itemBuilder: (context, index) {
                                  return Container(
                                    child: QOptions(
                                        controller.quizList[index].question,
                                        controller.quizList[index].option1,
                                        controller.quizList[index].option2,
                                        controller.quizList[index].option3,
                                        controller.quizList[index].option4,
                                        controller.quizList[index].correctAnswer ==
                                                "option1"
                                            ? controller.quizList[index].option1
                                            : controller.quizList[index]
                                                        .correctAnswer ==
                                                    "option2"
                                                ? controller.quizList[index].option2
                                                : controller.quizList[index]
                                                            .correctAnswer ==
                                                        "option3"
                                                    ? controller
                                                        .quizList[index].option3
                                                    : controller.quizList[index]
                                                                .correctAnswer ==
                                                            "option4"
                                                        ? controller
                                                            .quizList[index].option4
                                                        : "",
                                        index,
                                        controller),
                                  );
                                }),
                            ElevatedButton(
                                onPressed: () {
                                  controller.count /
                                              controller.quizList.length *
                                              100 >
                                          50
                                      ? controller.applyJob()
                                      : showSuccessDialog(context);
                                },
                                child: Container(
                                  child: Text("Continue"),
                                ))
                          ],
                        ),
                      )
                    : Container(
                        height: Get.height,
                        width: Get.width,
                        child: LoadingBar.spinkit,
                      ),
              ),
            ),
          ],
        );
      }),
    );
  }

  showSuccessDialog(BuildContext context) {
    AlertDialog alert = AlertDialog(
      content: Container(
        padding: EdgeInsets.all(30),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Text(
              "Your Score",
              textAlign: TextAlign.center,
            ),
            SizedBox(
              height: 20,
            ),
            Text("${controller.count}",
                style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold),
                textAlign: TextAlign.center),
            SizedBox(
              height: 20,
            ),
            Text(
              controller.count / controller.quizList.length * 100 > 50
                  ? "Congrats! Your application for this job has been applied successfully"
                  : "Sorry you didn't pass the test. Please try again",
              textAlign: TextAlign.center,
            ),
            SizedBox(
              height: 40,
            ),
            ElevatedButton(
                onPressed: () {
                  Get.back();
                  1.seconds;
                  Get.offAllNamed(Routes.HOME);
                },
                child: Text("Done!"))
          ],
        ),
      ),
    );
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

}
