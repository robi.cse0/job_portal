import 'dart:async';
import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:job_portal/app/data/model/model_quiz.dart';
import 'package:job_portal/app/data/network/apis.dart';
import 'package:job_portal/app/data/network/http_service.dart';
import 'package:job_portal/app/routes/app_pages.dart';
import 'package:job_portal/app/shared_prefrence/pref_keys.dart';
import 'package:job_portal/app/shared_prefrence/pref_manager.dart';
import 'package:job_portal/app/utils/snack_bar.dart';

class QuizController extends GetxController {

  HttpService _httpService = Get.put(HttpService());
  RxBool isLoading = false.obs;
  var quizList = <ModelQuiz>[].obs;
  RxString timeLeft = "".obs;
  RxInt totalTime = 0.obs;
  RxString second = "".obs;
  RxString minute = "0".obs;
  List<Map<String, dynamic>> countData = [];

  RxInt count = 0.obs;
  String categoryId = "0";
  String jobId = "0";
  late Timer _timer;

  @override
  void onInit() {
    super.onInit();
    categoryId = Get.arguments[0];
    jobId = Get.arguments[1];
  }

  @override
  void onReady() {
    super.onReady();
    getQuiz(categoryId);
  }

  @override
  void onClose() {
    super.dispose();
    if(_timer != null){
      _timer.cancel();
    }
    Get.delete<QuizController>();
  }

  getQuiz(String categoryId) async {

    isLoading.value = true;

    try {
      var apiResponse = await _httpService.getRequest(
          Apis.GET_QUIZ + "?id=${categoryId}");

      List<dynamic> quizData = apiResponse.data;

      quizList.value = List<ModelQuiz>.from(
          quizData.map((i) => ModelQuiz.fromJson(i)));

      if (apiResponse.statusCode == 200) {
        isLoading.value = false;
        totalTime.value = quizList.length * 30;
        startTimer();

      }else {
        isLoading.value = false;
        SnackBarWidget.showMessage("Server Error");
      }
    } on DioError catch (e) {
      isLoading.value = false;
      SnackBarWidget.showMessage(e.message);
    } catch (e) {
      isLoading.value = false;
      SnackBarWidget.showMessage(e.toString());
      throw e;
    }
  }

  applyJob() async {

    isLoading.value = true;

    Map<String, dynamic> data = {
      'job_id': jobId,
      'user_id': PrefManager.getString(PrefKeys.USER_ID)
    };

    try {
      var apiResponse = await _httpService
          .postRequest(Apis.APPLY_JOBS, data: data, isFormData: true);

      Map<String, dynamic> mapData = jsonDecode(apiResponse.toString());
      if(mapData['status'] == 200){
        isLoading.value = false;
        showSuccessDialog();
      }else{
        isLoading.value = false;
        SnackBarWidget.showMessage("Server Error");
      }
    } on DioError catch (e) {
      isLoading.value = false;
      SnackBarWidget.showMessage(e.message);
    } catch (e) {
      isLoading.value = false;
      SnackBarWidget.showMessage(e.toString());
      throw e;
    }
  }

  showSuccessDialog() {
    AlertDialog alert = AlertDialog(
      content: Container(
        padding: EdgeInsets.all(30),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Text(
              "Your Score",
              textAlign: TextAlign.center,
            ),
            SizedBox(
              height: 20,
            ),
            Text("${count}",
                style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold),
                textAlign: TextAlign.center),
            SizedBox(
              height: 20,
            ),
            Text(
              quizList.length * 100 > 50
                  ? "Congrats! Your application for this job has been applied successfully"
                  : "Sorry you didn't pass the test. Please try again",
              textAlign: TextAlign.center,
            ),
            SizedBox(
              height: 40,
            ),
            ElevatedButton(
                onPressed: () {
                  Get.back();
                  1.seconds;
                  Get.offAllNamed(Routes.HOME);
                },
                child: Text("Done!"))
          ],
        ),
      ),
    );
    showDialog(
      barrierDismissible: false,
      context: Get.context!,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  startTimer(){
    _timer = Timer.periodic(Duration(seconds: 1), (Timer timer) {
      totalTime--;

      if(totalTime == 0){
        applyJob();
      }
      minute.value = (totalTime / 60).floor().toString();
      second.value = (totalTime.value - (int.parse(minute.value) * 60)).toString();
      timeLeft.value = ("Times left : "+"$minute : ${second.value.length == 1 ? '0'+second.value : second.value}");

    });
  }

  scoreCount(Map<String, dynamic> map){

    count.value = 0;
    var foundKey;

    for (var element in countData){

      if(map["index"] == element["index"]){
        foundKey = element;
      }
    }
    countData.remove(foundKey);
    countData.add(map);
    for (var element in countData){
      count.value += int.parse(element["value"]);
    }
  }


}
