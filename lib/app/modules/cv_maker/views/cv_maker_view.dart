import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:job_portal/app/modules/cv_maker/widget/acheivment_view.dart';
import 'package:job_portal/app/modules/cv_maker/widget/education%20_view.dart';
import 'package:job_portal/app/modules/cv_maker/widget/exprience_view.dart';
import 'package:job_portal/app/modules/cv_maker/widget/language_view.dart';
import 'package:job_portal/app/modules/cv_maker/widget/skills_view.dart';
import 'package:job_portal/app/routes/app_pages.dart';
import 'package:job_portal/app/shared_prefrence/pref_keys.dart';
import 'package:job_portal/app/shared_prefrence/pref_manager.dart';
import 'package:job_portal/app/utils/app_constant.dart';
import 'package:job_portal/app/utils/loading_bar.dart';
import 'package:percent_indicator/linear_percent_indicator.dart';

import '../controllers/cv_maker_controller.dart';

class CvMakerView extends GetView<CvMakerController> {

  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Obx((){
      return Scaffold(
        appBar: AppBar(
          title: Text('MY CV'),
          centerTitle: true,
          actions: [
            Center(
              child: InkWell(
                onTap: (){
                  controller.submitCV();
                },
                child: Container(
                    margin: EdgeInsets.only(right: 15),
                    child: Text("Submit", style: TextStyle(
                        fontSize: 18,
                        fontWeight: FontWeight.bold
                    ),)),
              ),
            )
          ],
        ),
        body: controller.isLoading.isFalse ? SingleChildScrollView(
          child: Container(
            padding: EdgeInsets.all(16),
            child: Form(
              key: _formKey,
              child: Column(
                children: [
                  Container(
                    child: Text(
                      "Your CV Progress :",
                      style: TextStyle(
                          color: Colors.black,
                          fontSize: 18,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.fromLTRB(0, 20, 0, 20),
                    child: LinearPercentIndicator(
                      lineHeight: 18.0,
                      percent: AppConstant.cv_progress.value / 100,
                      center: Text(
                        "${AppConstant.cv_progress.value}%",
                        style: new TextStyle(fontSize: 12.0),
                      ),
                      trailing: Image.asset(
                        AppConstant.cv_progress.value > 80
                            ? "assets/images/green_click.png"
                            : "assets/images/ic_info_red.png",
                        height: 25,
                        width: 25,
                      ),
                      linearStrokeCap: LinearStrokeCap.roundAll,
                      backgroundColor: Colors.grey,
                      progressColor: Colors.blue,
                    ),
                  ),
                  //name
                  Container(
                    margin: EdgeInsets.only(top: 10, bottom: 10),
                    child: TextFormField(
                      controller: controller.nameController,
                      decoration: InputDecoration(
                        enabledBorder: OutlineInputBorder(
                          borderSide: const BorderSide(
                              color: Colors.grey, width: 1.5),
                          borderRadius: BorderRadius.circular(5.0),
                        ),
                        border: const OutlineInputBorder(),
                        filled: true,
                        fillColor: Colors.white,
                        focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                              color: Colors.grey, width: 2.0),
                          borderRadius: BorderRadius.circular(5.0),
                        ),
                        label: Text("Name"),
                        labelStyle: const TextStyle(fontFamily: ""),
                      ),
                      keyboardType: TextInputType.emailAddress,
                      onChanged: (value) {
                        PrefManager.putValue(PrefKeys.CV_NAME, value);
                      },
                      validator: (value) {
                        if (value!.isEmpty) {
                          return 'Please enter your name';
                        }
                      },
                    ),
                  ),
                  //Email
                  Container(
                    margin: EdgeInsets.only(top: 10, bottom: 10),
                    child: TextFormField(
                      controller: controller.emailController,
                      decoration: InputDecoration(
                        enabledBorder: OutlineInputBorder(
                          borderSide: const BorderSide(
                              color: Colors.grey, width: 1.5),
                          borderRadius: BorderRadius.circular(5.0),
                        ),
                        border: const OutlineInputBorder(),
                        filled: true,
                        fillColor: Colors.white,
                        focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                              color: Colors.grey, width: 2.0),
                          borderRadius: BorderRadius.circular(5.0),
                        ),
                        label: Text("Email"),
                        labelStyle: const TextStyle(fontFamily: ""),
                      ),
                      keyboardType: TextInputType.emailAddress,
                      onChanged: (value) {
                        PrefManager.putValue(PrefKeys.CV_EMAIL, value);
                      },
                      validator: (value) {
                        if (!value!.isEmail) {
                          return 'Please enter a valid email';
                        }
                      },
                    ),
                  ),
                  //Address
                  Container(
                    margin: EdgeInsets.only(top: 10, bottom: 10),
                    child: TextFormField(
                      controller: controller.addrressController,
                      decoration: InputDecoration(
                        enabledBorder: OutlineInputBorder(
                          borderSide: const BorderSide(
                              color: Colors.grey, width: 1.5),
                          borderRadius: BorderRadius.circular(5.0),
                        ),
                        border: const OutlineInputBorder(),
                        filled: true,
                        fillColor: Colors.white,
                        focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                              color: Colors.grey, width: 2.0),
                          borderRadius: BorderRadius.circular(5.0),
                        ),
                        label: Text("Address"),
                        labelStyle: const TextStyle(fontFamily: ""),
                      ),
                      keyboardType: TextInputType.emailAddress,
                      onChanged: (value) {
                        PrefManager.putValue(PrefKeys.CV_ADDRESS, value);
                      },
                      validator: (value) {
                        // if (value!.isEmpty) {
                        //   return 'Please enter your address';
                        // }
                      },
                    ),
                  ),
                  //Mobile
                  Container(
                    margin: EdgeInsets.only(top: 10, bottom: 10),
                    child: TextFormField(
                      controller: controller.mobileController,
                      decoration: InputDecoration(
                        enabledBorder: OutlineInputBorder(
                          borderSide: const BorderSide(
                              color: Colors.grey, width: 1.5),
                          borderRadius: BorderRadius.circular(5.0),
                        ),
                        border: const OutlineInputBorder(),
                        filled: true,
                        fillColor: Colors.white,
                        focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                              color: Colors.grey, width: 2.0),
                          borderRadius: BorderRadius.circular(5.0),
                        ),
                        label: Text("Phone number"),
                        labelStyle: const TextStyle(fontFamily: ""),
                      ),
                      keyboardType: TextInputType.number,
                      onChanged: (value) {
                        PrefManager.putValue(PrefKeys.CV_MOBILE, value);
                      },
                      validator: (value) {
                        // if (value!.isEmpty) {
                        //   return 'Please enter your address';
                        // }
                      },
                    ),
                  ),
                  //Career objective
                  Container(
                    margin: EdgeInsets.only(top: 10, bottom: 10),
                    child: TextFormField(
                      controller: controller.objectiveController,
                      decoration: InputDecoration(
                        enabledBorder: OutlineInputBorder(
                          borderSide: const BorderSide(
                              color: Colors.grey, width: 1.5),
                          borderRadius: BorderRadius.circular(5.0),
                        ),
                        border: const OutlineInputBorder(),
                        filled: true,
                        fillColor: Colors.white,
                        focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                              color: Colors.grey, width: 2.0),
                          borderRadius: BorderRadius.circular(5.0),
                        ),
                        label: Text("Career Objective"),
                        labelStyle: const TextStyle(fontFamily: ""),
                      ),
                      keyboardType: TextInputType.text,
                      onChanged: (value) {
                        PrefManager.putValue(PrefKeys.CV_OBJECTIVE, value);
                      },
                      validator: (value) {
                        // if (value!.isEmpty) {
                        //   return 'Please enter your address';
                        // }
                      },
                    ),
                  ),
                  //Skills
                  SkillsView(),
                  SizedBox(height: 20,),
                  ExperienceView(),
                  SizedBox(height: 20,),
                  AcheivementView(),
                  SizedBox(height: 20,),
                  EducationView(),
                  SizedBox(height: 20,),
                  LanguageView(),
                  SizedBox(height: 60,),
                ],
              ),
            ),
          ),
        ) :
        Container(
          height: Get.height,
          width: Get.width,
          child: LoadingBar.spinkit,
        ),
      );
    });
  }
}
