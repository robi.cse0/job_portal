import 'package:get/get.dart';

import '../controllers/cv_maker_controller.dart';

class CvMakerBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<CvMakerController>(
      () => CvMakerController(),
    );
  }
}
