import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:job_portal/app/data/model/model_experience.dart';
import 'package:job_portal/app/data/model/model_language.dart';
import 'package:job_portal/app/modules/cv_maker/controllers/cv_maker_controller.dart';
import 'package:job_portal/app/shared_prefrence/pref_keys.dart';
import 'package:job_portal/app/shared_prefrence/pref_manager.dart';

class LanguageView extends StatefulWidget {
  const LanguageView({Key? key}) : super(key: key);

  @override
  _LanguageViewState createState() => _LanguageViewState();
}

List<ModelLanguage> LanguageList = [];

class _LanguageViewState extends State<LanguageView> {

  var controller = Get.put(CvMakerController());

  @override
  void initState() {
    super.initState();
    LanguageList = getList();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          SizedBox(
            height: 20,
          ),
          Container(
            margin: EdgeInsets.only(left: 5, right: 16),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  "Language Proficiency",
                  style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                ),
                GestureDetector(
                    onTap: () {
                      _displayTextInputDialog(context);
                      setState(() {});
                    },
                    child: Icon(Icons.add))
              ],
            ),
          ),
          Divider(
            color: Colors.black,
          ),
          SizedBox(
            height: 10,
          ),
          ListView.builder(
              physics: NeverScrollableScrollPhysics(),
              shrinkWrap: true,
              itemCount: LanguageList.length,
              itemBuilder: (context, index) {
                return Container(
                  padding: EdgeInsets.all(8),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        width : 20,
                        child: Text("${index+1}. ", style: TextStyle(fontSize: 17, fontWeight: FontWeight.bold),),
                      ),
                      SizedBox(
                        width: 10,
                      ),
                      Expanded(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Container(
                              child: Text(
                                LanguageList[index].languageName,
                                style: TextStyle(fontSize: 17, fontWeight: FontWeight.bold),
                              ),
                            ),
                            SizedBox(height: 10,),
                            Container(
                              child: Text(
                                "Fluency : ${LanguageList[index].proficiency}",
                                style: TextStyle(fontSize: 16, fontWeight: FontWeight.normal),
                              ),
                            ),
                            SizedBox(height: 5,),
                          ],
                        ),
                      ),
                      SizedBox(
                        width: 10,
                      ),
                      GestureDetector(
                          onTap: () {
                            removeData(LanguageList[index]);
                          },
                          child: Icon(Icons.delete)
                      )
                    ],
                  ),
                );
              })
        ],
      ),
    );
  }

  TextEditingController nameController = TextEditingController();
  TextEditingController positionController = TextEditingController();

  var _formKey = GlobalKey<FormFieldState>();

  Future<void> _displayTextInputDialog(BuildContext context) async {
    return showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text('Add Institution'),
            content: Form(
              key: _formKey,
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  TextFormField(
                    controller: nameController,
                    decoration: InputDecoration(hintText: "language name"),
                    validator: (val){
                      if(val!.isEmpty){
                        return "Please enter language name";
                      }
                    },
                  ),
                  TextFormField(
                    controller: positionController,
                    decoration: InputDecoration(hintText: "Fluency"),
                    validator: (val){
                      if(val!.isEmpty){
                        return "Please write how fluent you are";
                      }
                    },
                  ),
                ],
              ),
            ),
            actions: <Widget>[
              GestureDetector(
                onTap: () {
                  Get.back();
                },
                child: Container(
                  padding: EdgeInsets.all(5.0),
                  child: Text(
                    "CANCEL",
                    style: TextStyle(color: Colors.deepOrangeAccent),
                  ),
                ),
              ),
              GestureDetector(
                onTap: () {
                    updateAndSaveList(
                        nameController.text,
                      positionController.text,
                    );
                    nameController.text = "";
                    positionController.text = "";

                  Get.back();
                },
                child: Container(
                  padding: EdgeInsets.only(left: 5.0, right: 20),
                  child: Text(
                    "ADD",
                    style: TextStyle(color: Colors.blue),
                  ),
                ),
              ),
            ],
          );
        });
  }

  updateAndSaveList(String name, String proficiency) {

    List<ModelLanguage> list = getList();

      ModelLanguage item = ModelLanguage(languageName: name, proficiency: proficiency);

    list.add(item);
    controller.language_proficiency.value = listToJson(list);
    LanguageList = list;
    setState(() {});
  }

  getList() {
    String data = controller.language_proficiency.value;
    if(data == ""){
      controller.language_proficiency.value = "[]";
      return <ModelLanguage>[];
    }else{
      return jsonToList(data);
    }
  }

  jsonToList(String data){
    List<ModelLanguage> finalList = [];
    var list = json.decode(data)
        .map((data) => ModelLanguage.fromJson(data))
        .toList();

    for(int i = 0 ; i < list.length ; i++){
      finalList.add(list[i]);
    }

    return finalList;
  }

  listToJson(List<ModelLanguage> list){
    return jsonEncode(list);
  }

  removeData(ModelLanguage item) {
    int index = 0;
    List<ModelLanguage> list = getList();
    for(int i = 0 ; i < list.length ; i++){
      if(item.languageName == list[i].languageName){
        index = i ;
      }
    }
    list.removeAt(index);
    controller.language_proficiency.value = listToJson(list);
    LanguageList = list;
    setState(() {});
  }
}
