import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:job_portal/app/data/model/model_experience.dart';
import 'package:job_portal/app/modules/cv_maker/controllers/cv_maker_controller.dart';
import 'package:job_portal/app/shared_prefrence/pref_keys.dart';
import 'package:job_portal/app/shared_prefrence/pref_manager.dart';

class ExperienceView extends StatefulWidget {
  const ExperienceView({Key? key}) : super(key: key);

  @override
  _ExperienceViewState createState() => _ExperienceViewState();
}

List<ModelExperience> experienceList = [];

class _ExperienceViewState extends State<ExperienceView> {

  var controller = Get.put(CvMakerController());

  @override
  void initState() {
    super.initState();
    experienceList = getList();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          SizedBox(
            height: 20,
          ),
          Container(
            margin: EdgeInsets.only(left: 5, right: 16),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  "Professional Experience",
                  style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                ),
                GestureDetector(
                    onTap: () {
                      _displayTextInputDialog(context);
                      setState(() {});
                    },
                    child: Icon(Icons.add))
              ],
            ),
          ),
          Divider(
            color: Colors.black,
          ),
          SizedBox(
            height: 10,
          ),
          ListView.builder(
              physics: NeverScrollableScrollPhysics(),
              shrinkWrap: true,
              itemCount: experienceList.length,
              itemBuilder: (context, index) {
                return Container(
                  padding: EdgeInsets.all(8),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        width : 20,
                        child: Text("${index+1}. ", style: TextStyle(fontSize: 17, fontWeight: FontWeight.bold),),
                      ),
                      SizedBox(
                        width: 10,
                      ),
                      Expanded(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Container(
                              child: Text(
                                experienceList[index].name,
                                style: TextStyle(fontSize: 17, fontWeight: FontWeight.bold),
                              ),
                            ),
                            SizedBox(height: 10,),
                            Container(
                              child: Text(
                                "Position : ${experienceList[index].position}",
                                style: TextStyle(fontSize: 16, fontWeight: FontWeight.normal),
                              ),
                            ),
                            SizedBox(height: 5,),
                            Container(
                              child: Text(
                                "Working period : ${experienceList[index].period}",
                                style: TextStyle(fontSize: 16, fontWeight: FontWeight.normal),
                              ),
                            ),
                            SizedBox(height: 5,),
                            Container(
                              child: Text(
                                "Job details : ${experienceList[index].project}",
                                style: TextStyle(fontSize: 16, fontWeight: FontWeight.normal),
                              ),
                            ),
                          ],
                        ),
                      ),
                      SizedBox(
                        width: 10,
                      ),
                      GestureDetector(
                          onTap: () {
                            removeData(experienceList[index]);
                          },
                          child: Icon(Icons.delete)
                      )
                    ],
                  ),
                );
              })
        ],
      ),
    );
  }

  TextEditingController nameController = TextEditingController();
  TextEditingController positionController = TextEditingController();
  TextEditingController periodController = TextEditingController();
  TextEditingController jobDetailsController = TextEditingController();

  var _formKey = GlobalKey<FormFieldState>();

  Future<void> _displayTextInputDialog(BuildContext context) async {
    return showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text('Add Professional experience'),
            content: Form(
              key: _formKey,
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  TextFormField(
                    controller: nameController,
                    decoration: InputDecoration(hintText: "Company name"),
                    validator: (val){
                      if(val!.isEmpty){
                        return "Please enter company name";
                      }
                    },
                  ),
                  TextFormField(
                    controller: positionController,
                    decoration: InputDecoration(hintText: "Position"),
                    validator: (val){
                      if(val!.isEmpty){
                        return "Please enter position";
                      }
                    },
                  ),
                  TextFormField(
                    controller: periodController,
                    decoration: InputDecoration(hintText: "Working Period"),
                    validator: (val){
                      if(val!.isEmpty){
                        return "Please enter working period";
                      }
                    },
                  ),
                  TextFormField(
                    controller: jobDetailsController,
                    keyboardType: TextInputType.multiline,
                    decoration: InputDecoration(hintText: "Job Details"),
                    validator: (val){
                      if(val!.isEmpty){
                        return "Please enter Job details";
                      }
                    },
                  ),

                ],
              ),
            ),
            actions: <Widget>[
              GestureDetector(
                onTap: () {
                  Get.back();
                },
                child: Container(
                  padding: EdgeInsets.all(5.0),
                  child: Text(
                    "CANCEL",
                    style: TextStyle(color: Colors.deepOrangeAccent),
                  ),
                ),
              ),
              GestureDetector(
                onTap: () {
                    updateAndSaveList(
                        nameController.text,
                      positionController.text,
                      periodController.text,
                      jobDetailsController.text,
                    );
                    nameController.text = "";
                    positionController.text = "";
                    periodController.text = "";
                    jobDetailsController.text = "";

                  Get.back();
                },
                child: Container(
                  padding: EdgeInsets.only(left: 5.0, right: 20),
                  child: Text(
                    "ADD",
                    style: TextStyle(color: Colors.blue),
                  ),
                ),
              ),
            ],
          );
        });
  }

  updateAndSaveList(String name, String position, String period, String details) {

    List<ModelExperience> list = getList();

      ModelExperience item = ModelExperience(
          name: name,
          position: position,
          period: period,
          project: details
      );

    list.add(item);
    controller.experience.value = listToJson(list);
    experienceList = list;
    setState(() {});
  }

  getList() {
    String data = controller.experience.value;
    if(data == ""){
      controller.experience.value = "[]";
      return <ModelExperience>[];
    }else{
      return jsonToList(data);
    }
  }

  jsonToList(String data){
    List<ModelExperience> finalList = [];
    var list = json.decode(data)
        .map((data) => ModelExperience.fromJson(data))
        .toList();

    for(int i = 0 ; i < list.length ; i++){
      finalList.add(list[i]);
    }

    return finalList;
  }

  listToJson(List<ModelExperience> list){
    return jsonEncode(list);
  }

  removeData(ModelExperience item) {
    int index = 0;
    List<ModelExperience> list = getList();
    for(int i = 0 ; i < list.length ; i++){
      if(item.name == list[i].name){
        index = i ;
      }
    }
    list.removeAt(index);
    controller.experience.value = listToJson(list);
    experienceList = list;
    setState(() {});
  }
}
