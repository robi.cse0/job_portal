import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:job_portal/app/data/model/model_education.dart';
import 'package:job_portal/app/modules/cv_maker/controllers/cv_maker_controller.dart';
import 'package:job_portal/app/shared_prefrence/pref_keys.dart';
import 'package:job_portal/app/shared_prefrence/pref_manager.dart';

class EducationView extends StatefulWidget {
  const EducationView({Key? key}) : super(key: key);

  @override
  _EducationViewState createState() => _EducationViewState();
}

List<ModelEducation> EducationList = [];

class _EducationViewState extends State<EducationView> {

  var controller = Get.put(CvMakerController());

  @override
  void initState() {
    super.initState();
    EducationList = getList();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          SizedBox(
            height: 20,
          ),
          Container(
            margin: EdgeInsets.only(left: 5, right: 16),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  "Education",
                  style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                ),
                GestureDetector(
                    onTap: () {
                      _displayTextInputDialog(context);
                      setState(() {});
                    },
                    child: Icon(Icons.add))
              ],
            ),
          ),
          Divider(
            color: Colors.black,
          ),
          SizedBox(
            height: 10,
          ),
          ListView.builder(
              physics: NeverScrollableScrollPhysics(),
              shrinkWrap: true,
              itemCount: EducationList.length,
              itemBuilder: (context, index) {
                return Container(
                  padding: EdgeInsets.all(8),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        width : 20,
                        child: Text("${index+1}. ", style: TextStyle(fontSize: 17, fontWeight: FontWeight.bold),),
                      ),
                      SizedBox(
                        width: 10,
                      ),
                      Expanded(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Container(
                              child: Text(
                                EducationList[index].level,
                                style: TextStyle(fontSize: 17, fontWeight: FontWeight.bold),
                              ),
                            ),
                            SizedBox(height: 10,),
                            Container(
                              child: Text(
                                "Institute : ${EducationList[index].instituteName}",
                                style: TextStyle(fontSize: 16, fontWeight: FontWeight.normal),
                              ),
                            ),
                            SizedBox(height: 5,),
                            Container(
                              child: Text(
                                "Year : ${EducationList[index].year}",
                                style: TextStyle(fontSize: 16, fontWeight: FontWeight.normal),
                              ),
                            ),
                            SizedBox(height: 5,),
                            Container(
                              child: Text(
                                "GPA/CGPA : ${EducationList[index].gpaCgpa}",
                                style: TextStyle(fontSize: 16, fontWeight: FontWeight.normal),
                              ),
                            ),
                          ],
                        ),
                      ),
                      SizedBox(
                        width: 10,
                      ),
                      GestureDetector(
                          onTap: () {
                            removeData(EducationList[index]);
                          },
                          child: Icon(Icons.delete)
                      )
                    ],
                  ),
                );
              })
        ],
      ),
    );
  }

  TextEditingController nameController = TextEditingController();
  TextEditingController positionController = TextEditingController();
  TextEditingController periodController = TextEditingController();
  TextEditingController jobDetailsController = TextEditingController();

  var _formKey = GlobalKey<FormFieldState>();

  Future<void> _displayTextInputDialog(BuildContext context) async {
    return showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text('Add skills'),
            content: Form(
              key: _formKey,
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  TextFormField(
                    controller: nameController,
                    decoration: InputDecoration(hintText: "SSC/HSC/BSC"),
                    validator: (val){
                      if(val!.isEmpty){
                        return "Please enter a valid text";
                      }
                    },
                  ),
                  TextFormField(
                    controller: positionController,
                    decoration: InputDecoration(hintText: "Institute name"),
                    validator: (val){
                      if(val!.isEmpty){
                        return "Please enter a valid text";
                      }
                    },
                  ),
                  TextFormField(
                    controller: periodController,
                    decoration: InputDecoration(hintText: "Passing year"),
                    validator: (val){
                      if(val!.isEmpty){
                        return "Please enter a valid text";
                      }
                    },
                  ),
                  TextFormField(
                    controller: jobDetailsController,
                    keyboardType: TextInputType.multiline,
                    decoration: InputDecoration(hintText: "GPA/CGPA"),
                    validator: (val){
                      if(val!.isEmpty){
                        return "Please enter a valid text";
                      }
                    },
                  ),

                ],
              ),
            ),
            actions: <Widget>[
              GestureDetector(
                onTap: () {
                  Get.back();
                },
                child: Container(
                  padding: EdgeInsets.all(5.0),
                  child: Text(
                    "CANCEL",
                    style: TextStyle(color: Colors.deepOrangeAccent),
                  ),
                ),
              ),
              GestureDetector(
                onTap: () {
                    updateAndSaveList(
                        nameController.text,
                      positionController.text,
                      periodController.text,
                      jobDetailsController.text,
                    );
                    nameController.text = "";
                    positionController.text = "";
                    periodController.text = "";
                    jobDetailsController.text = "";

                  Get.back();
                },
                child: Container(
                  padding: EdgeInsets.only(left: 5.0, right: 20),
                  child: Text(
                    "ADD",
                    style: TextStyle(color: Colors.blue),
                  ),
                ),
              ),
            ],
          );
        });
  }

  updateAndSaveList(String name, String position, String period, String details) {

    List<ModelEducation> list = getList();

      ModelEducation item = ModelEducation(
          level: name,
          instituteName: position,
          year: period,
          gpaCgpa: details
      );

    list.add(item);
    controller.education.value = listToJson(list);
    EducationList = list;
    print(_prettyPrint(EducationList));
    setState((){});
  }

  String _prettyPrint(jsonObject) {
    var encoder = JsonEncoder.withIndent('    ');
    return encoder.convert(jsonObject);
  }

  getList() {
    String data = controller.education.value;
    if(data == ""){
      controller.education.value = "[]";
      return <ModelEducation>[];
    }else{
      return jsonToList(data);
    }
  }

  jsonToList(String data){
    List<ModelEducation> finalList = [];
    var list = json.decode(data)
        .map((data) => ModelEducation.fromJson(data))
        .toList();

    for(int i = 0 ; i < list.length ; i++){
      finalList.add(list[i]);
    }

    return finalList;
  }

  listToJson(List<ModelEducation> list){
    return jsonEncode(list);
  }

  removeData(ModelEducation item) {
    int index = 0;
    List<ModelEducation> list = getList();
    for(int i = 0 ; i < list.length ; i++){
      if(item.level == list[i].level){
        index = i ;
      }
    }
    list.removeAt(index);
    controller.education.value = listToJson(list);
    EducationList = list;
    setState(() {});
  }
}
