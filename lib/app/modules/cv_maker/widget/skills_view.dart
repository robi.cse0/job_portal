import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:job_portal/app/modules/cv_maker/controllers/cv_maker_controller.dart';
import 'package:job_portal/app/shared_prefrence/pref_keys.dart';
import 'package:job_portal/app/shared_prefrence/pref_manager.dart';

class SkillsView extends StatefulWidget {
  const SkillsView({Key? key}) : super(key: key);

  @override
  _SkillsViewState createState() => _SkillsViewState();
}

List<String> dataList = [];

class _SkillsViewState extends State<SkillsView> {

  var controller = Get.put(CvMakerController());

  @override
  void initState() {
    super.initState();
    dataList = getList();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          SizedBox(
            height: 20,
          ),
          Container(
            margin: EdgeInsets.only(left: 5, right: 16),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  "Skills",
                  style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                ),
                GestureDetector(
                    onTap: () {
                      _displayTextInputDialog(context);
                      setState(() {});
                    },
                    child: Icon(Icons.add))
              ],
            ),
          ),
          Divider(
            color: Colors.black,
          ),
          SizedBox(
            height: 10,
          ),
          ListView.builder(
              physics: NeverScrollableScrollPhysics(),
              shrinkWrap: true,
              itemCount: dataList.length,
              itemBuilder: (context, index) {
                return Container(
                  padding: EdgeInsets.all(8),
                  child: Row(
                    children: [
                      Container(
                        height: 15,
                        width: 15,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(
                            Radius.circular(8.0),
                          ),
                          color: Colors.grey,
                        ),
                      ),
                      SizedBox(
                        width: 10,
                      ),
                      Expanded(
                        child: Container(
                          child: Text(
                            dataList[index],
                            style: TextStyle(fontSize: 16),
                          ),
                        ),
                      ),
                      SizedBox(
                        width: 10,
                      ),
                      GestureDetector(
                          onTap: () {
                            removeData(dataList[index]);
                          },
                          child: Icon(Icons.delete))
                    ],
                  ),
                );
              })
        ],
      ),
    );
  }

  TextEditingController _textFieldController = TextEditingController();

  Future<void> _displayTextInputDialog(BuildContext context) async {
    return showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text('Add skills'),
            content: TextField(
              controller: _textFieldController,
              decoration: InputDecoration(hintText: ""),
            ),
            actions: <Widget>[
              GestureDetector(
                onTap: () {
                  Get.back();
                },
                child: Container(
                  padding: EdgeInsets.all(5.0),
                  child: Text(
                    "CANCEL",
                    style: TextStyle(color: Colors.deepOrangeAccent),
                  ),
                ),
              ),
              GestureDetector(
                onTap: () {
                  if (!_textFieldController.text.isEmpty) {
                    updateAndSaveList(_textFieldController.text);
                    _textFieldController.text = "";
                  }
                  Get.back();
                },
                child: Container(
                  padding: EdgeInsets.only(left: 5.0, right: 20),
                  child: Text(
                    "ADD",
                    style: TextStyle(color: Colors.blue),
                  ),
                ),
              ),
            ],
          );
        });
  }

  updateAndSaveList(String text) {
    List<String> list = getList();
    list.add(text);
    controller.skills.value = listToJson(list);
    dataList = list;
    setState(() {});
  }

  getList() {
    String data = controller.skills.value;
    if(data == ""){
      controller.skills.value = "[]";
      return <String>[];
    }else{
      return jsonToList(data);
    }
  }

  listToJson(List<String> list){
    var encoder = JsonEncoder();
    return encoder.convert(list);
  }

  jsonToList(String data){
    List<String> finalList = [];
    var list = json.decode(data);
    for(int i = 0; i < list.length ; i++){
      finalList.add(list[i]);
    }
    return finalList;
  }

  removeData(String text) {
    List<String> list = getList();
    list.remove(text);
    controller.skills.value = listToJson(list);
    dataList = list;
    setState(() {});
  }
}
