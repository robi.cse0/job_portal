import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:job_portal/app/data/network/apis.dart';
import 'package:job_portal/app/data/network/http_service.dart';
import 'package:job_portal/app/modules/cv_preview/controllers/cv_preview_controller.dart';
import 'package:job_portal/app/shared_prefrence/pref_keys.dart';
import 'package:job_portal/app/shared_prefrence/pref_manager.dart';
import 'package:job_portal/app/utils/snack_bar.dart';

class CvMakerController extends GetxController {
  var cvPreviewController = Get.put(CvPreviewController());
  HttpService _httpService = Get.put(HttpService());
  RxBool isLoading = false.obs;

  var nameController = TextEditingController();
  var emailController = TextEditingController();
  var addrressController = TextEditingController();
  var mobileController = TextEditingController();
  var objectiveController = TextEditingController();

  RxString skills = "".obs;
  RxString experience = "".obs;
  RxString achievements = "".obs;
  RxString education = "".obs;
  RxString language_proficiency = "".obs;

  @override
  void onInit() {
    super.onInit();
    nameController.text = PrefManager.getString(PrefKeys.CV_NAME);
    emailController.text = PrefManager.getString(PrefKeys.CV_EMAIL);
    addrressController.text = PrefManager.getString(PrefKeys.CV_ADDRESS);
    mobileController.text = PrefManager.getString(PrefKeys.CV_MOBILE);
    objectiveController.text = PrefManager.getString(PrefKeys.CV_OBJECTIVE);
    skills.value = PrefManager.getString(PrefKeys.CV_SKILLS);
    experience.value = PrefManager.getString(PrefKeys.CV_EXPERIENCE);
    achievements.value = PrefManager.getString(PrefKeys.CV_ACHEIVEMENTS);
    education.value = PrefManager.getString(PrefKeys.CV_EDUCATION);
    language_proficiency.value = PrefManager.getString(PrefKeys.CV_LANGUAGE);
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    cvPreviewController.onReady();
  }

  submitCV() async {
    isLoading.value = true;

    Map<String, dynamic> data = {
      'name': nameController.text,
      'email': emailController.text,
      'phone': mobileController.text,
      'address': addrressController.text,
      'career_objective': objectiveController.text,
      'skills': skills.value,
      'experience': experience.value,
      'achievements': achievements.value,
      'education': education.value,
      'language_proficiency': language_proficiency.value
    };

    try {
      var apiResponse = await _httpService.postRequest(
          Apis.UPDATE_CV + "?id=${PrefManager.getString(PrefKeys.USER_ID)}",
          data: data,
          isFormData: true);

      Map<String, dynamic> mapData = jsonDecode(apiResponse.toString());
      if (mapData['status'] == 200) {
        isLoading.value = false;
        PrefManager.putString(PrefKeys.CV_NAME, nameController.text);
        PrefManager.putString(PrefKeys.CV_EMAIL, emailController.text);
        PrefManager.putString(PrefKeys.CV_ADDRESS, addrressController.text);
        PrefManager.putString(PrefKeys.CV_MOBILE, mobileController.text);
        PrefManager.putString(PrefKeys.CV_OBJECTIVE, objectiveController.text);
        PrefManager.putString(PrefKeys.CV_SKILLS, skills.value);
        PrefManager.putString(PrefKeys.CV_EXPERIENCE, experience.value);
        PrefManager.putString(PrefKeys.CV_ACHEIVEMENTS, achievements.value);
        PrefManager.putString(PrefKeys.CV_EDUCATION, education.value);
        PrefManager.putString(PrefKeys.CV_LANGUAGE, language_proficiency.value);
        cvPreviewController.updateProgress();
      }else {
        isLoading.value = false;
        SnackBarWidget.showMessage("Server Error");
      }
    } on DioError catch (e) {
      isLoading.value = false;
      SnackBarWidget.showMessage(e.message);
    } catch (e) {
      isLoading.value = false;
      SnackBarWidget.showMessage(e.toString());
      throw e;
    }
  }
}
