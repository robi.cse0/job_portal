import 'package:get/get.dart';
import 'package:job_portal/app/data/model/model_job_post.dart';

import '../../search/controllers/search_controller.dart';

class JobDetailsController extends GetxController {

  var searchController = Get.put(SearchController());

  late ModelJobPost modelJobPost;

  @override
  void onInit() {
    super.onInit();
    modelJobPost = Get.arguments[0];
    searchController.modelSearchItems.location.forEach((element) {
      if(modelJobPost.company.locationId == element.id.toString()){
        modelJobPost.company.locationId = element.name;
      }
    });
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}
}
