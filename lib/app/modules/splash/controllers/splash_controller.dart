import 'package:get/get.dart';
import 'package:job_portal/app/routes/app_pages.dart';
import 'package:job_portal/app/shared_prefrence/pref_keys.dart';
import 'package:job_portal/app/shared_prefrence/pref_manager.dart';

class SplashController extends GetxController {

  @override
  void onInit() {
    super.onInit();
    gotoNextPage();
  }

  @override
  void onReady() {
    super.onReady();
    // gotoNextPage();
  }

  @override
  void onClose() {}

  gotoNextPage(){
    Future.delayed(const Duration(seconds: 3), () {
      if (PrefManager.getBoolean(PrefKeys.LOGIN_STATUS) == true) {
        Get.offAndToNamed(Routes.HOME);
      } else {
        Get.offAndToNamed(Routes.LOGIN);
      }
    });
  }
}
