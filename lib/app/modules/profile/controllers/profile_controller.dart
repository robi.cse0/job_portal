import 'package:get/get.dart';
import 'package:job_portal/app/shared_prefrence/pref_keys.dart';
import 'package:job_portal/app/shared_prefrence/pref_manager.dart';

class ProfileController extends GetxController {

  RxString imageUrl = "".obs;

  @override
  void onInit() {
    super.onInit();
    imageUrl.value = "https://mazharsany.xyz/jobs-today/storage/app/"+PrefManager.getString(PrefKeys.USER_IMAGE);
  }

  @override
  void onReady() {
    super.onReady();
    update();
  }

  @override
  void onClose() {}
}
