import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:job_portal/app/routes/app_pages.dart';
import 'package:job_portal/app/shared_prefrence/pref_keys.dart';
import 'package:job_portal/app/shared_prefrence/pref_manager.dart';

import '../controllers/profile_controller.dart';

class ProfileView extends GetView<ProfileController> {
  @override
  Widget build(BuildContext context) {
    return Obx(() {
      return Scaffold(
        backgroundColor: Colors.white,
        body: Container(
          padding: EdgeInsets.all(16),
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(
                  height: 20,
                ),
                Center(
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(75.0),
                    child: SizedBox(
                      height: 150.0,
                      width: 150.0,
                      child: controller.imageUrl.value == ""?
                          Image.asset("assets/images/avatar.png"):
                      Image.network(
                        controller.imageUrl.value,
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                Center(
                  child: Text(
                    PrefManager.getString(PrefKeys.USER_NAME),
                    style: TextStyle(fontSize: 22, fontWeight: FontWeight.bold),
                  ),
                ),
                SizedBox(
                  height: 40,
                ),
                Container(
                  child: Row(
                    children: [
                      Icon(Icons.person),
                      SizedBox(
                        width: 10,
                      ),
                      Text(
                        "Personal Info :",
                        style: TextStyle(
                            fontSize: 18, fontWeight: FontWeight.bold),
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                Divider(
                  height: 2,
                  color: Colors.black,
                ),
                SizedBox(
                  height: 20,
                ),
                Container(
                  child: Row(
                    children: [
                      Icon(
                        Icons.email,
                        size: 20,
                      ),
                      SizedBox(
                        width: 10,
                      ),
                      Text(
                        "Email : ",
                        style: TextStyle(
                            fontSize: 16, fontWeight: FontWeight.bold),
                      ),
                      SizedBox(
                        width: 10,
                      ),
                      Text(
                        PrefManager.getString(PrefKeys.USER_EMAIL),
                        style: TextStyle(
                            fontSize: 16, fontWeight: FontWeight.normal),
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                Container(
                  child: Row(
                    children: [
                      Icon(
                        Icons.phone,
                        size: 20,
                      ),
                      SizedBox(
                        width: 10,
                      ),
                      Text(
                        "Mobile :",
                        style: TextStyle(
                            fontSize: 16, fontWeight: FontWeight.bold),
                      ),
                      SizedBox(
                        width: 10,
                      ),
                      Text(
                        PrefManager.getString(PrefKeys.USER_MOBILE),
                        style: TextStyle(
                            fontSize: 16, fontWeight: FontWeight.normal),
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                Container(
                  child: Row(
                    children: [
                      Icon(
                        Icons.access_time,
                        size: 20,
                      ),
                      SizedBox(
                        width: 10,
                      ),
                      Text(
                        "Date of birth :",
                        style: TextStyle(
                            fontSize: 16, fontWeight: FontWeight.bold),
                      ),
                      SizedBox(
                        width: 10,
                      ),
                      Text(
                        PrefManager.getString(PrefKeys.USER_DOB)
                                    .toString()
                                    .length >
                                11
                            ? PrefManager.getString(PrefKeys.USER_DOB)
                                .toString()
                                .substring(0, 10)
                            : PrefManager.getString(PrefKeys.USER_DOB)
                                .toString(),
                        style: TextStyle(
                            fontSize: 16, fontWeight: FontWeight.normal),
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: 40,
                ),
                Container(
                  child: Row(
                    children: [
                      Icon(Icons.settings),
                      SizedBox(
                        width: 10,
                      ),
                      Text(
                        "Settings :",
                        style: TextStyle(
                            fontSize: 18, fontWeight: FontWeight.bold),
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                Divider(
                  height: 2,
                  color: Colors.black,
                ),
                SizedBox(
                  height: 20,
                ),
                GestureDetector(
                  onTap: () {
                    Get.toNamed(Routes.CV_PREVIEW);
                  },
                  child: Container(
                    padding: EdgeInsets.all(8.0),
                    child: Text(
                      "MY CV",
                      style: TextStyle(
                          color: Colors.lightBlueAccent,
                          fontSize: 17,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                ),
                GestureDetector(
                  onTap: () {
                    Get.toNamed(Routes.UPDATE_PROFILE);
                  },
                  child: Container(
                    padding: EdgeInsets.all(8.0),
                    child: Text(
                      "Update Personal Info",
                      style: TextStyle(
                          color: Colors.lightBlueAccent,
                          fontSize: 17,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                ),
                GestureDetector(
                  onTap: () {
                    Get.toNamed(Routes.UPDATE_PROFILE_PICTURE);
                  },
                  child: Container(
                    padding: EdgeInsets.all(8.0),
                    child: Text(
                      "Update Profile picture",
                      style: TextStyle(
                          color: Colors.lightBlueAccent,
                          fontSize: 17,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                ),
                GestureDetector(
                  onTap: () {
                    Get.toNamed(Routes.HELP_LINE);
                  },
                  child: Container(
                    padding: EdgeInsets.all(8.0),
                    child: Text(
                      "Help Center",
                      style: TextStyle(
                          color: Colors.lightBlueAccent,
                          fontSize: 17,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                ),
                GestureDetector(
                  onTap: () {
                    PrefManager.putValue(PrefKeys.LOGIN_STATUS, false);
                    PrefManager.clear();
                    Get.offAllNamed(Routes.LOGIN);
                  },
                  child: Container(
                    padding: EdgeInsets.all(8.0),
                    child: Text(
                      "Logout",
                      style: TextStyle(
                          color: Colors.lightBlueAccent,
                          fontSize: 17,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      );
    });
  }
}
